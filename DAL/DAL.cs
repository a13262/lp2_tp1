﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using Common;

namespace DAL
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="Common.ICrud{T}" />
    /// <seealso cref="System.IDisposable" />
    public abstract class DAL<T>:ICrud<T>
    {
        #region Estado

        /// <summary>
        /// The serializer
        /// </summary>
        private DataContractJsonSerializer serializer;

        /// <summary>
        /// The dir
        /// </summary>
        private readonly string dir = typeof(T).Name + "s";

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DAL{T}" /> class.
        /// </summary>
        public DAL()
        {
            try
            {
                if (!Directory.Exists(Dir))
                {
                    Directory.CreateDirectory(Dir);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            Serializer = new DataContractJsonSerializer(typeof(T));
            
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the serializer.
        /// </summary>
        /// <value>
        /// The serializer.
        /// </value>
        public DataContractJsonSerializer Serializer
        {
            get { return serializer; }
            set { serializer = value; }
        }

        /// <summary>
        /// Gets the dir.
        /// </summary>
        /// <value>
        /// The dir.
        /// </value>
        public string Dir
        {
            get { return dir; }
            
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public abstract bool Create(T obj);

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public abstract T Read(Guid id);

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public abstract List<T> Read();

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public abstract bool Update(T obj);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public abstract bool Delete(Guid id);

        /// <summary>
        /// Existses the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">File Exists Exception</exception>
        public bool Exists(Guid id)
        {
            try
            {
                return File.Exists(Dir + "/" + id + ".json");
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the dir of.
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <returns></returns>
        protected string GetDirOf<K>()
        {
            return typeof(K).Name + "s";
        }

        /// <summary>
        /// Existses the specified identifier.
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="Exception">File Exists Exception</exception>
        public bool Exists<K>(Guid id)
        {
            try
            {
                return File.Exists(GetDirOf<K>() + "/" + id + ".json");
            }
            catch (Exception e)
            {
                throw;
            }
        }

        #endregion



    }
}