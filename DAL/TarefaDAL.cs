﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using BO;

namespace DAL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DAL.DAL{BO.Tarefa}" />
    /// <seealso cref="Tarefa" />
    public class TarefaDAL:DAL<Tarefa>
    {
        #region Estado

        /// <summary>
        /// The identifier projecto
        /// </summary>
        private Guid idProjecto;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TarefaDAL" /> class.
        /// </summary>
        /// <param name="idProjecto">The identifier projecto.</param>
        public TarefaDAL(Guid idProjecto)
        {
            IdProjecto = idProjecto;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TarefaDAL" /> class.
        /// </summary>
        public TarefaDAL()
        {

        }

        /// <summary>
        /// Gets or sets the identifier projecto.
        /// </summary>
        /// <value>
        /// The identifier projecto.
        /// </value>
        public Guid IdProjecto
        {
            get { return idProjecto; }
            set { idProjecto = value; }
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public override bool Create(Tarefa obj)
        {
            obj.IdProjecto = IdProjecto;
            
            string fullPath = Dir + "/" + obj.Id + ".json";

            try
            {
                using (FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    Serializer.WriteObject(fs, obj);
                }
                
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override Tarefa Read(Guid id)
        {
            string fullPath = Dir + "/" + id + ".json";
            long fileSize = new FileInfo(fullPath).Length;
            if (!File.Exists(fullPath) || fileSize == 0)
            {
                return null;
            }

            try
            {
                using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                {
                   Tarefa r = Serializer.ReadObject(fs) as Tarefa;
                    if (r.Id == id)
                    {
                        return r;
                    }
                }
             
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
            
            return null;
        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public override List<Tarefa> Read()
        {
            List<Tarefa> list = new List<Tarefa>();

            try
            {
                string[] files = Directory.GetFiles(Dir, "*.json").Select(Path.GetFileName).ToArray();

                if (IdProjecto == Guid.Empty)
                {
                    
                    foreach (string file in files)
                    {
                        string fullPath = Dir + "/" + file;
                        long fileSize = new FileInfo(fullPath).Length;
                        if (fileSize != 0)
                        {
                            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                            {
                                Tarefa r = Serializer.ReadObject(fs) as Tarefa;
                                list.Add(r);
                            }
                        }
                    }
                }
                else
                {
                    foreach (string file in files)
                    {
                        string fullPath = Dir + "/" + file;
                        long fileSize = new FileInfo(fullPath).Length;
                        if (fileSize != 0)
                        {
                            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                            {
                                Tarefa r = Serializer.ReadObject(fs) as Tarefa;
                                if(r.IdProjecto == IdProjecto)
                                list.Add(r);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return list;
        }

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public override bool Update(Tarefa obj)
        {
            try
            {
                string fullPath = Dir + "/" + obj.Id + ".json";
                long fileSize = new FileInfo(fullPath).Length;
                if (!File.Exists(fullPath) || fileSize == 0)
                {
                    return false;
                }

            
                Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override bool Delete(Guid id)
        {
            try
            {

                string fullPath = Dir + "/" + id + ".json";

                if (!File.Exists(fullPath))
                {
                    return false;
                }

                using (FileStream fs = new FileStream(fullPath, FileMode.Truncate))
                {
                    
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        #endregion



    }
}