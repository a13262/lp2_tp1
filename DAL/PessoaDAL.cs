﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using BO;

namespace DAL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DAL.DAL{BO.Pessoa}" />
    public class PessoaDAL:DAL<Pessoa>
    {
        #region Estado

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PessoaDAL"/> class.
        /// </summary>
        public PessoaDAL()
        {
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public override bool Create(Pessoa obj)
        {
            string fullPath = Dir + "/" + obj.Id + ".json";

            try
            {
                using (FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    Serializer.WriteObject(fs, obj);
                }
                
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override Pessoa Read(Guid id)
        {
           
            try
            {
                string fullPath = Dir + "/" + id + ".json";
                long fileSize = new FileInfo(fullPath).Length;
                if (!File.Exists(fullPath) || fileSize == 0)
                {
                    return null;
                }
                using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                {
                   Pessoa r = Serializer.ReadObject(fs) as Pessoa;
                    if (r.Id == id)
                    {
                        return r;
                    }
                }
             
            }
            catch (Exception e)
            {
                throw;
            }
            
            return null;
        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public override List<Pessoa> Read()
        {
            List<Pessoa> list = new List<Pessoa>();

            try
            {
               string[] files  = Directory.GetFiles(Dir, "*.json").Select(Path.GetFileName).ToArray();
                foreach (string file in files)
                {
                    string fullPath = Dir + "/" + file;
                    long fileSize = new FileInfo(fullPath).Length;
                    if (fileSize!=0)
                    {
                        using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                        {
                            Pessoa p = Serializer.ReadObject(fs) as Pessoa;
                            list.Add(p);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return list;
        }

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public override bool Update(Pessoa obj)
        {
            
            try
            {
                string fullPath = Dir + "/" + obj.Id + ".json";
                long fileSize = new FileInfo(fullPath).Length;
                if (!File.Exists(fullPath) || fileSize == 0)
                {
                    return false;
                }


                Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override bool Delete(Guid id)
        {
            
            try
            {
                string fullPath = Dir + "/" + id + ".json";

                if (!File.Exists(fullPath))
                {
                    return false;
                }

                using (FileStream fs = new FileStream(fullPath, FileMode.Truncate))
                {
                    
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }
        #endregion



    }
}