﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using BO;

namespace DAL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DAL.DAL{BO.Recurso}" />
    public class RecursoDAL:DAL<Recurso>
    {
        #region Estado

        /// <summary>
        /// The identifier elemento assoc
        /// </summary>
        private Guid idElementoAssoc;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RecursoDAL"/> class.
        /// </summary>
        /// <param name="idElementoAssoc">The identifier elemento assoc.</param>
        public RecursoDAL(Guid idElementoAssoc)
        {
            IdElementoAssoc = idElementoAssoc;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecursoDAL"/> class.
        /// </summary>
        public RecursoDAL()
        {
        }

        /// <summary>
        /// Gets or sets the identifier elemento assoc.
        /// </summary>
        /// <value>
        /// The identifier elemento assoc.
        /// </value>
        public Guid IdElementoAssoc
        {
            get { return idElementoAssoc; }
            set { idElementoAssoc = value; }
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public override bool Create(Recurso obj)
        {
            string fullPath = Dir + "/" + obj.Id + ".json";

            try
            {
                using (FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    Serializer.WriteObject(fs, obj);
                }
                
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override Recurso Read(Guid id)
        {
            try
            {
                string fullPath = Dir + "/" + id + ".json";
                long fileSize = new FileInfo(fullPath).Length;
                if (!File.Exists(fullPath) || fileSize == 0)
                {
                    return null;
                }
                using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                {
                   Recurso r = Serializer.ReadObject(fs) as Recurso;
                    if (r.Id == id)
                    {
                        return r;
                    }
                }
             
            }
            catch (Exception e)
            {
                throw;
            }
            
            return null;
        }

        /// <summary>
        /// Reads the specified get all.
        /// </summary>
        /// <param name="getAll">if set to <c>true</c> [get all].</param>
        /// <returns></returns>
        public List<Recurso> Read(bool getAll)
        {
            if (getAll)
            {
                List<Recurso> list = new List<Recurso>();
                try
                {
                    string[] files = Directory.GetFiles(Dir, "*.json").Select(Path.GetFileName).ToArray();
                    foreach (string file in files)
                    {
                        string fullPath = Dir + "/" + file;
                        long fileSize = new FileInfo(fullPath).Length;
                        if (fileSize != 0)
                        {
                            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                            {
                                Recurso r = Serializer.ReadObject(fs) as Recurso;
                                list.Add(r);
                            }
                        }
                    }
                    return list;
                }
                catch (Exception e)
                {
                    throw;
                }
            }
            return Read();
        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public override List<Recurso> Read()
        {
            List<Recurso> list = new List<Recurso>();

            try
            {
                string[] files = Directory.GetFiles(Dir, "*.json").Select(Path.GetFileName).ToArray();

                foreach (string file in files)
                    {
                        string fullPath = Dir + "/" + file;
                        long fileSize = new FileInfo(fullPath).Length;
                        if (fileSize != 0)
                        {
                            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite))
                            {
                                Recurso r = Serializer.ReadObject(fs) as Recurso;
                                if(r.IdElementoAssociado == IdElementoAssoc)
                                list.Add(r);
                            }
                        }
                    }
            }
            catch (Exception e)
            {
                throw;
            }

            return list;
        }

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public override bool Update(Recurso obj)
        {
            try
            {
                string fullPath = Dir + "/" + obj.Id + ".json";
                long fileSize = new FileInfo(fullPath).Length;
                if (!File.Exists(fullPath) || fileSize == 0)
                {
                    return false;
                }
                Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override bool Delete(Guid id)
        {
            try
            {
                string fullPath = Dir + "/" + id + ".json";

                if (!File.Exists(fullPath))
                {
                    return false;
                }

                using (FileStream fs = new FileStream(fullPath, FileMode.Truncate))
                {
                    
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        #endregion



    }
}