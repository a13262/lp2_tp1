﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using BO;
using Common;
using DAL;

namespace BLL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Recurso}" />
    public class RecursoBLL:ICrud<Recurso>
    {
        #region Estado

        /// <summary>
        /// The identifier elemento assoc
        /// </summary>
        private Guid idElementoAssoc;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RecursoBLL"/> class.
        /// </summary>
        /// <param name="idElementoAssoc">The identifier elemento assoc.</param>
        public RecursoBLL(Guid idElementoAssoc)
        {
            IdElementoAssoc = idElementoAssoc;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecursoBLL"/> class.
        /// </summary>
        public RecursoBLL()
        {
        }

        /// <summary>
        /// Gets or sets the identifier elemento assoc.
        /// </summary>
        /// <value>
        /// The identifier elemento assoc.
        /// </value>
        public Guid IdElementoAssoc
        {
            get { return idElementoAssoc; }
            set
            {
                try
                {
                    var rdal = new RecursoDAL();
                    if (rdal.Exists<Projecto>(value) || rdal.Exists<Tarefa>(value))
                    {
                        idElementoAssoc = value;
                    }
                    else
                    {
                        idElementoAssoc = Guid.Empty;
                    }
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos

        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Recurso obj)
        {
            try
            {
                var rdal = new RecursoDAL();

                if (rdal.Exists(obj.Id)) return false;

                return rdal.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Recurso Read(Guid id)
        {
            try
            {
                var rdal = new RecursoDAL();

                if (!rdal.Exists(id)) return null;

                return rdal.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }
            
        }

        /// <summary>
        /// Reads the specified get all.
        /// </summary>
        /// <param name="getAll">if set to <c>true</c> [get all].</param>
        /// <returns></returns>
        public List<Recurso> Read(bool getAll)
        {
            try
            {
                return new RecursoDAL(IdElementoAssoc).Read(getAll);
            }
            catch (Exception e)
            {
                throw;
            }
            
        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Recurso> Read()
        {
            try
            {
                return new RecursoDAL(IdElementoAssoc).Read();
            }
            catch (Exception e)
            {
                throw;
            }
            
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Recurso obj)
        {
            try
            {
                var rdal = new RecursoDAL();

                if (!rdal.Exists(obj.Id)) return false;

                return rdal.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            try
            {
                var rdal = new RecursoDAL();

                if (!rdal.Exists(id) || HasDependecy(id)) return false;

                return rdal.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Frees the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static bool Free(Guid id)
        {
            try
            {
                var rdal = new RecursoDAL();

                if (!rdal.Exists(id)) return false;

                Recurso r = rdal.Read(id);

                r.IdElementoAssociado = Guid.Empty;

                return rdal.Update(r);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Frees all.
        /// </summary>
        /// <param name="idElementoAssoc">The identifier elemento assoc.</param>
        /// <returns></returns>
        public static bool FreeAll(Guid idElementoAssoc)
        {
            var rdal = new RecursoDAL(idElementoAssoc);
            try
            {
                if (
                !rdal.Exists<Tarefa>(idElementoAssoc) &&
                !rdal.Exists<Projecto>(idElementoAssoc)
                ) return false;
            
                foreach (Recurso r in rdal.Read())
                {
                    Free(r.Id);
                }

            }
            catch (Exception e)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Determines whether the specified identifier has dependecy.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified identifier has dependecy; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasDependecy(Guid id)
        {
            try
            {
                var rdal = new RecursoDAL();

                if (id == Guid.Empty || !rdal.Exists(id)) return false;

                Recurso r = rdal.Read(id);

                return !(r.IdElementoAssociado == Guid.Empty);
            }
            catch (Exception e)
            {
                throw;
            }


        }

        #endregion



    }
}