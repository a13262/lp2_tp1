﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using BO;
using Common;
using DAL;

namespace BLL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Tarefa}" />
    public class TarefaBLL:ICrud<Tarefa>
    {
        #region Estado

        /// <summary>
        /// The identifier projecto
        /// </summary>
        private Guid idProjecto;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TarefaBLL"/> class.
        /// </summary>
        /// <param name="idProjecto">The identifier projecto.</param>
        public TarefaBLL(Guid idProjecto)
        {
            IdProjecto = idProjecto;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TarefaBLL"/> class.
        /// </summary>
        public TarefaBLL()
        {

        }

        #endregion

        #region Propriedades
        /// <summary>
        /// Gets or sets the identifier projecto.
        /// </summary>
        /// <value>
        /// The identifier projecto.
        /// </value>
        public Guid IdProjecto
        {
            get { return idProjecto; }
            set
            {
                var tdal = new TarefaDAL();
                if (tdal.Exists<Projecto>(value))
                {
                    idProjecto = value;
                }
                else
                {
                    idProjecto = Guid.Empty;
                }
                
            }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Tarefa obj)
        {
            try
            {
                var tdal = new TarefaDAL(IdProjecto);

                if (tdal.Exists(obj.Id)) return false;

                return tdal.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Tarefa Read(Guid id)
        {
            try
            {
                var tdal = new TarefaDAL();

                if (!tdal.Exists(id)) return null;

                return tdal.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Tarefa> Read()
        {
            try
            {
                var tdal = new TarefaDAL(IdProjecto);
                return tdal.Read();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Tarefa obj)
        {
            try
            {
                var tdal = new TarefaDAL(IdProjecto);

                if (!tdal.Exists(obj.Id)) return false;

                return tdal.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {

            try
            {
                var tdal = new TarefaDAL();

                if (!tdal.Exists(id) || HasDependecy(id)) return false;

                return RecursoBLL.FreeAll(id) && tdal.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Determines whether the specified identifier has dependecy.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified identifier has dependecy; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasDependecy(Guid id)
        {
            try
            {
                var tdal = new TarefaDAL();

                if (id == Guid.Empty || !tdal.Exists(id)) return false;

                Tarefa t = tdal.Read(id);

                foreach (Tarefa tarefa in tdal.Read())
                {
                    if (tarefa.IdTarefaPai == t.Id)
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        #endregion



    }
}