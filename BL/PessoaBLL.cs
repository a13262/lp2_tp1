﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using BO;
using Common;
using DAL;

namespace BLL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Pessoa}" />
    public class PessoaBLL:ICrud<Pessoa>
    {
        #region Estado

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PessoaBLL"/> class.
        /// </summary>
        public PessoaBLL()
        {
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Pessoa obj)
        {
            try
            {
                var pdal = new PessoaDAL();

                if (pdal.Exists(obj.Id)) return false;

                return pdal.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Pessoa Read(Guid id)
        {
            try
            {
                var pdal = new PessoaDAL();

                if (!pdal.Exists(id)) return null;

                return pdal.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Pessoa> Read()
        {
            try
            {
                var pdal = new PessoaDAL();
                return pdal.Read();
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Pessoa obj)
        {
            try
            {
                var pdal = new PessoaDAL();

                if (!pdal.Exists(obj.Id)) return false;

                return pdal.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            try
            {
                var pdal = new PessoaDAL();

                if (!pdal.Exists(id) || HasDependecy(id)) return false;

                return pdal.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Determines whether the specified identifier has dependecy.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified identifier has dependecy; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasDependecy(Guid id)
        {
            try
            {
                var pdal = new PessoaDAL();

                if (id == Guid.Empty || !pdal.Exists(id)) return false;

                Recurso r = pdal.Read(id);

                return !(r.IdElementoAssociado == Guid.Empty);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Frees the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static bool Free(Guid id)
        {
            try
            {
                var pdal = new PessoaDAL();

                if (id == Guid.Empty || !pdal.Exists(id)) return false;

                Pessoa p = pdal.Read(id);

                p.IdElementoAssociado = Guid.Empty;

                return pdal.Update(p);
            }
            catch (Exception e)
            {
                throw;
            }
            
        }
        #endregion



    }
}