﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using BO;
using Common;
using DAL;

namespace BLL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Projecto}" />
    public class ProjectoBLL:ICrud<Projecto>
    {
        #region Estado

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectoBLL"/> class.
        /// </summary>
        public ProjectoBLL()
        {
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos

        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Projecto obj)
        {
            try
            {
                var pdal = new ProjectoDAL();

                if (pdal.Exists(obj.Id)) return false;

                return pdal.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Projecto Read(Guid id)
        {
            try
            {
                var pdal = new ProjectoDAL();

                if (!pdal.Exists(id)) return null;

                return pdal.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Projecto> Read()
        {
            try
            {
                var pdal = new ProjectoDAL();
                return pdal.Read();
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Projecto obj)
        {
            try
            {
                var pdal = new ProjectoDAL();

                if (!pdal.Exists(obj.Id)) return false;

                return pdal.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            var pdal = new ProjectoDAL();
            bool personFreed = false;
            bool tasksDeleted = true;

            try
            {
                if (!pdal.Exists(id)) return false;

                TarefaBLL tbll = new TarefaBLL(id);
                
                List<Tarefa> list = tbll.Read();
               
                foreach (Tarefa t in list)
                {
                    if (!tbll.Delete(t.Id))
                    {
                        tasksDeleted = false;
                        break;
                    }
                }

                if (pdal.Read(id).IdResponsavel == Guid.Empty)
                {
                    personFreed = true;
                }
                else
                {
                    personFreed = PessoaBLL.Free(pdal.Read(id).IdResponsavel);
                }

                return RecursoBLL.FreeAll(id) && personFreed && tasksDeleted && pdal.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Relatorioes the projecto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static RelatorioProjecto RelatorioProjecto(Guid id)
        {

            var relatorio = new RelatorioProjecto();
            var pdal = new ProjectoDAL();
            var tbll = new TarefaBLL(id);
            var pbll = new PessoaBLL();
            var rbll = new RecursoBLL(id);

            try
            {
                if (!pdal.Exists(id) || id == Guid.Empty) return null;

                Projecto p = pdal.Read(id);

                relatorio.Nome = p.Nome;
                relatorio.NomeResponsavel = pbll.Read(p.IdResponsavel).Nome;
                relatorio.Tipo = p.Tipo.ToString();
                relatorio.Recursos = rbll.Read();


                foreach (Tarefa t in tbll.Read())
                {
                    rbll.IdElementoAssoc = t.Id;
                    foreach (Recurso r in rbll.Read())
                    {
                        relatorio.Recursos.Add(r);
                    }

                    if (DateTime.Now > t.Fim && t.Estado == Tarefa.EstadoTarefa.Iniciada)
                        relatorio.TarefasEmAtraso.Add(t);

                    if (t.Estado == Tarefa.EstadoTarefa.Terminada)
                        relatorio.TarefasTerminadas.Add(t);
                }

                return relatorio;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        #endregion



    }
}