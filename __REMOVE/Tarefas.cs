﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;

namespace LP2_TP1
{
    /// <summary>
    /// 
    /// </summary>
    public class Tarefas
    {
        #region Estado

        /// <summary>
        /// The lista tarefas
        /// </summary>
        private Tarefa[] listaTarefas;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefas"/> class.
        /// </summary>
        public Tarefas()
        {
        }

        #endregion

        #region Propriedades

        /*
        public Tarefa[] Lista
        {
            get { return listaTarefas; }
            set { listaTarefas = value; }
        }

        */
        #endregion

        #region Métodos
        /// <summary>
        /// Adicionas the tarefa.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool AdicionaTarefa(Tarefa t) { throw new NotImplementedException();}
        /// <summary>
        /// Adicionas the tarefa.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <param name="tarefaPai">The tarefa pai.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool AdicionaTarefa(Tarefa t, Tarefa tarefaPai) { throw new NotImplementedException();}
        /// <summary>
        /// Removes the tarefa.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool RemoveTarefa(Tarefa t) { throw new NotImplementedException();}
        #endregion



    }
}