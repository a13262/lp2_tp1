﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

namespace LP2_TP1
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="LP2_TP1.Recurso" />
    public class Pessoa:Recurso
    {
        #region Estado

        /// <summary>
        /// The nome
        /// </summary>
        private string nome;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Pessoa"/> class.
        /// </summary>
        /// <param name="nome">The nome.</param>
        public Pessoa(string nome)
        {
            Nome = nome;
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        #endregion

        #region Métodos

        #endregion



    }
}