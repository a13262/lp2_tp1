﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;

namespace LP2_TP1
{
    /// <summary>
    /// 
    /// </summary>
    public class Projectos
    {
        #region Estado

        /// <summary>
        /// The lista projectos
        /// </summary>
        private Projecto[] listaProjectos;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Projectos"/> class.
        /// </summary>
        public Projectos()
        {
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the lista projectos.
        /// </summary>
        /// <value>
        /// The lista projectos.
        /// </value>
        public Projecto[] ListaProjectos
        {
            get { return listaProjectos; }
            set { listaProjectos = value; }
        }

        #endregion

        #region Métodos
        /// <summary>
        /// Adicionas the projecto.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool AdicionaProjecto(Projecto p) { throw new NotImplementedException();}
        /// <summary>
        /// Removes the projecto.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool RemoveProjecto(Projecto p) { throw new NotImplementedException();}
        #endregion



    }
}