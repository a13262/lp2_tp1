﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

namespace LP2_TP1
{
    /// <summary>
    /// Representa um recurso
    /// </summary>
    public abstract class Recurso
    {
        #region EstadoTarefa

        /// <summary>
        /// The identifier
        /// </summary>
        private int id;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Recurso"/> class.
        /// </summary>
        public Recurso()
        {
        }

        #endregion

        #region Propriedades
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        #endregion

        #region Métodos

        #endregion



    }
}