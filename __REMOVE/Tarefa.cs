﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

namespace LP2_TP1
{
    /// <summary>
    /// 
    /// </summary>
    public class Tarefa
    {
        /// <summary>
        /// 
        /// </summary>
        public enum EstadoTarefa
        {
            /// <summary>
            /// The terminada
            /// </summary>
            Terminada,
            /// <summary>
            /// The iniciada
            /// </summary>
            Iniciada
        }

        #region EstadoTarefa

        /// <summary>
        /// The identifier
        /// </summary>
        private int id;
        /// <summary>
        /// The descricao
        /// </summary>
        private string descricao;
        /// <summary>
        /// The identifier tarefa pai
        /// </summary>
        private int idTarefaPai;
        /// <summary>
        /// The estado
        /// </summary>
        private EstadoTarefa estado;
        /// <summary>
        /// The lista recursos
        /// </summary>
        private Recursos listaRecursos;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefa"/> class.
        /// </summary>
        public Tarefa()
        {
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        /// <summary>
        /// Gets or sets the identifier tarefa pai.
        /// </summary>
        /// <value>
        /// The identifier tarefa pai.
        /// </value>
        public int IdTarefaPai
        {
            get { return idTarefaPai; }
            set { idTarefaPai = value; }
        }

        /// <summary>
        /// Gets or sets the estado.
        /// </summary>
        /// <value>
        /// The estado.
        /// </value>
        public EstadoTarefa Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        /// <summary>
        /// Gets or sets the lista recursos.
        /// </summary>
        /// <value>
        /// The lista recursos.
        /// </value>
        public Recursos ListaRecursos
        {
            get { return listaRecursos; }
            set { listaRecursos = value; }
        }

        #endregion

        #region Métodos

        #endregion



    }
}