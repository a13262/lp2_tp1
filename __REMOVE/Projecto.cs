﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;

namespace LP2_TP1
{
    /// <summary>
    /// 
    /// </summary>
    public class Projecto
    {
        /// <summary>
        /// 
        /// </summary>
        public enum TipoProjecto
        {
            /// <summary>
            /// The software
            /// </summary>
            Software,
            /// <summary>
            /// The engenharia civil
            /// </summary>
            EngenhariaCivil,
            /// <summary>
            /// The arquitectura
            /// </summary>
            Arquitectura
        }

        #region EstadoTarefa

        /// <summary>
        /// The identifier
        /// </summary>
        private int id;
        /// <summary>
        /// The nome
        /// </summary>
        private string nome;
        /// <summary>
        /// The tipo
        /// </summary>
        private TipoProjecto tipo;
        /// <summary>
        /// The inicio
        /// </summary>
        private DateTime inicio;
        /// <summary>
        /// The fim
        /// </summary>
        private DateTime fim;
        /// <summary>
        /// The lista tarefas
        /// </summary>
        private Tarefas listaTarefas;

        /// <summary>
        /// The lista recursos
        /// </summary>
        private Recursos listaRecursos;

        /// <summary>
        /// The responsavel
        /// </summary>
        private Pessoa responsavel;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Projecto" /> class.
        /// </summary>
        public Projecto()
        {
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        /// <summary>
        /// Gets or sets the inicio.
        /// </summary>
        /// <value>
        /// The inicio.
        /// </value>
        public DateTime Inicio
        {
            get { return inicio; }
            set { inicio = value; }
        }

        /// <summary>
        /// Gets or sets the fim.
        /// </summary>
        /// <value>
        /// The fim.
        /// </value>
        public DateTime Fim
        {
            get { return fim; }
            set { fim = value; }
        }

        /// <summary>
        /// Gets or sets the tipo.
        /// </summary>
        /// <value>
        /// The tipo.
        /// </value>
        public TipoProjecto Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        /// <summary>
        /// Gets or sets the lista tarefas.
        /// </summary>
        /// <value>
        /// The lista tarefas.
        /// </value>
        public Tarefas ListaTarefas
        {
            get { return listaTarefas; }
            set { listaTarefas = value; }
        }

        /// <summary>
        /// Gets or sets the lista recursos.
        /// </summary>
        /// <value>
        /// The lista recursos.
        /// </value>
        public Recursos ListaRecursos
        {
            get { return listaRecursos; }
            set { listaRecursos = value; }
        }

        /// <summary>
        /// Gets or sets the responsavel.
        /// </summary>
        /// <value>
        /// The responsavel.
        /// </value>
        public Pessoa Responsavel
        {
            get { return responsavel; }
            set { responsavel = value; }
        }

        #endregion

        #region Métodos

 
        
        #endregion



    }
}