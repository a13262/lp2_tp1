﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;

namespace LP2_TP1
{
    /// <summary>
    /// 
    /// </summary>
    public class Recursos
    {
        #region Estado

        /// <summary>
        /// The lista recursos
        /// </summary>
        private Recurso[] listaRecursos;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Recursos"/> class.
        /// </summary>
        public Recursos()
        {
        }

        #endregion

        #region Propriedades
        /*
        public Recurso[] ListaRecursos
        {
            get { return listaRecursos; }
            set { listaRecursos = value; }
        }
        */
        #endregion

        #region Métodos
        /// <summary>
        /// Adicionas the recurso.
        /// </summary>
        /// <param name="r">The r.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool AdicionaRecurso(Recurso r) { throw new NotImplementedException();}
        /// <summary>
        /// Removes the recurso.
        /// </summary>
        /// <param name="r">The r.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool RemoveRecurso(Recurso r) { throw new NotImplementedException();}
        #endregion



    }
}