﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BO
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "Projecto")]
    public class Projecto
    {
        /// <summary>
        /// 
        /// </summary>
        public enum TipoProjecto
        {
            /// <summary>
            /// The software
            /// </summary>
            Software,
            /// <summary>
            /// The engenharia civil
            /// </summary>
            EngenhariaCivil,
            /// <summary>
            /// The arquitectura
            /// </summary>
            Arquitectura
        }

        #region EstadoTarefa

        /// <summary>
        /// The identifier
        /// </summary>
        private System.Guid id;
        /// <summary>
        /// The nome
        /// </summary>
        private string nome;
        /// <summary>
        /// The tipo
        /// </summary>
        private TipoProjecto tipo;
        /// <summary>
        /// The inicio
        /// </summary>
        private DateTime inicio;
        /// <summary>
        /// The fim
        /// </summary>
        private DateTime fim;
        /// <summary>
        /// The idResponsavel
        /// </summary>
        private Guid idResponsavel;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Projecto" /> class.
        /// </summary>
        /// <param name="nome">The nome.</param>
        /// <param name="tipoProjecto">The tipo projecto.</param>
        /// <param name="inicio">The inicio.</param>
        /// <param name="fim">The fim.</param>
        /// <param name="listaTarefas">The lista tarefas.</param>
        /// <param name="listaRecursos">The lista recursos.</param>
        /// <param name="idResponsavel">The idResponsavel.</param>
        public Projecto(string nome, TipoProjecto tipoProjecto, DateTime inicio, DateTime fim, Guid idResponsavel)
        {
            Id = Guid.NewGuid();
            Nome = nome;
            Tipo = tipoProjecto;
            Inicio = inicio;
            Fim = fim;
            IdResponsavel = idResponsavel;
        }
        public Projecto(Guid id,string nome, TipoProjecto tipoProjecto, DateTime inicio, DateTime fim, Guid idResponsavel)
        {
            Id = id;
            Nome = nome;
            Tipo = tipoProjecto;
            Inicio = inicio;
            Fim = fim;
            IdResponsavel = idResponsavel;
        }

        public Projecto()
        {
            Id = Guid.NewGuid();
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember(Name = "Id_Projecto")]
        public System.Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        [DataMember(Name = "Nome_Projecto")]
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        /// <summary>
        /// Gets or sets the inicio.
        /// </summary>
        /// <value>
        /// The inicio.
        /// </value>
        [DataMember(Name = "Inicio_Projecto")]
        public DateTime Inicio
        {
            get { return inicio; }
            set { inicio = value; }
        }

        /// <summary>
        /// Gets or sets the fim.
        /// </summary>
        /// <value>
        /// The fim.
        /// </value>
        [DataMember(Name = "Fim_Projecto")]
        public DateTime Fim
        {
            get { return fim; }
            set { fim = value; }
        }

        /// <summary>
        /// Gets or sets the tipo.
        /// </summary>
        /// <value>
        /// The tipo.
        /// </value>
        [DataMember(Name = "Tipo_Projecto")]
        public TipoProjecto Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        /// <summary>
        /// Gets or sets the idResponsavel.
        /// </summary>
        /// <value>
        /// The idResponsavel.
        /// </value>
        [DataMember(Name = "Responsavel_Projecto")]
        public Guid IdResponsavel
        {
            get { return idResponsavel; }
            set { idResponsavel = value; }
        }

        #endregion

        #region Métodos

 
        
        #endregion



    }
}