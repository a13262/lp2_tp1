﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Runtime.Serialization;

namespace BO
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="BO.Recurso" />
    /// <seealso cref="Recurso" />
    [DataContract(Name = "Pessoa")]
    public class Pessoa:Recurso
    {
        #region Estado

        /// <summary>
        /// The nome
        /// </summary>
        private string nome;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Pessoa" /> class.
        /// </summary>
        /// <param name="descricao">The descricao.</param>
        /// <param name="nome">The nome.</param>
        public Pessoa(string descricao, string nome, Guid idElementoAssociado):base(descricao, idElementoAssociado)
        {
            Nome = nome;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pessoa"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="descricao">The descricao.</param>
        /// <param name="nome">The nome.</param>
        public Pessoa(Guid id,string descricao, string nome, Guid idElementoAssociado) : base(id, descricao, idElementoAssociado)
        {
            Nome = nome;
        }

        public Pessoa(Guid id)
        {
            Id = id;
        }

        public Pessoa():base()
        {

        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the nome.
        /// </summary>
        /// <value>
        /// The nome.
        /// </value>
        [DataMember(Name = "Nome_Pessoa")]
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        #endregion

        #region Métodos

        public override bool Equals(object obj)
        {
            if(obj is Pessoa p) return p.Id == Id;
            
            return false;
        }

        #endregion



    }
}