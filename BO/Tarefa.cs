﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BO
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "Tarefa")]
    public class Tarefa
    {
        /// <summary>
        /// 
        /// </summary>
        public enum EstadoTarefa
        {
            /// <summary>
            /// The iniciada
            /// </summary>
            Iniciada,
            /// <summary>
            /// The terminada
            /// </summary>
            Terminada

        }

        #region EstadoTarefa

        /// <summary>
        /// The identifier
        /// </summary>
        private Guid id;
        /// <summary>
        /// The descricao
        /// </summary>
        private string descricao;
        /// <summary>
        /// The identifier tarefa pai
        /// </summary>
        private Guid idTarefaPai;
        /// <summary>
        /// The estado
        /// </summary>
        private EstadoTarefa estado;

        /// <summary>
        /// The identifier projecto
        /// </summary>
        private Guid idProjecto;

        private DateTime inicio;

        private DateTime fim;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefa" /> class.
        /// </summary>
        /// <param name="descricao">The descricao.</param>
        /// <param name="idProjecto">The identifier projecto.</param>
        /// <param name="estadoTarefa">The estado tarefa.</param>
        public Tarefa(string descricao, Guid idProjecto, DateTime inicio, DateTime fim ,EstadoTarefa estadoTarefa = EstadoTarefa.Iniciada)
        {
            Id = Guid.NewGuid();
            IdTarefaPai = Id;
            Descricao = descricao;
            Estado = estadoTarefa;
            IdProjecto = idProjecto;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefa" /> class.
        /// </summary>
        /// <param name="descricao">The descricao.</param>
        /// <param name="idTarefaPai">The identifier tarefa pai.</param>
        /// <param name="idProjecto">The identifier projecto.</param>
        /// <param name="estadoTarefa">The estado tarefa.</param>
        public Tarefa(string descricao, Guid idTarefaPai, Guid idProjecto, DateTime inicio, DateTime fim, EstadoTarefa estadoTarefa = EstadoTarefa.Iniciada)
        {
            Id = Guid.NewGuid();
            IdTarefaPai = idTarefaPai;
            Descricao = descricao;
            IdProjecto = idProjecto;
            Estado = estadoTarefa;
            Inicio = inicio;
            Fim = fim;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefa" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="descricao">The descricao.</param>
        /// <param name="idProjecto">The identifier projecto.</param>
        /// <param name="estadoTarefa">The estado tarefa.</param>
        public Tarefa(Guid id, string descricao, Guid idProjecto , DateTime inicio, DateTime fim, EstadoTarefa estadoTarefa = EstadoTarefa.Iniciada)
        {
            Id = id;
            IdTarefaPai = Id;
            Descricao = descricao;
            Estado = estadoTarefa;
            IdProjecto = idProjecto;
            Inicio = inicio;
            Fim = fim;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefa" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="descricao">The descricao.</param>
        /// <param name="idTarefaPai">The identifier tarefa pai.</param>
        /// <param name="idProjecto">The identifier projecto.</param>
        /// <param name="estadoTarefa">The estado tarefa.</param>
        public Tarefa(Guid id, string descricao, Guid idTarefaPai, Guid idProjecto, DateTime inicio, DateTime fim, EstadoTarefa estadoTarefa = EstadoTarefa.Iniciada)
        {
            Id = id;
            IdTarefaPai = idTarefaPai;
            Descricao = descricao;
            Estado = estadoTarefa;
            IdProjecto = idProjecto;
            Inicio = inicio;
            Fim = fim;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefa"/> class.
        /// </summary>
        public Tarefa()
        {
            Id = Guid.NewGuid();
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember(Name = "Id_Tarefa")]
        public System.Guid Id
        {
            get { return id; }
            set { id = value;}
        }

        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        [DataMember(Name = "Descricao_Tarefa")]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        /// <summary>
        /// Gets or sets the identifier tarefa pai.
        /// </summary>
        /// <value>
        /// The identifier tarefa pai.
        /// </value>
        [DataMember(Name = "IdTarefaPai_Tarefa")]
        public Guid IdTarefaPai
        {
            get { return idTarefaPai; }
            set { idTarefaPai = value; }
        }

        /// <summary>
        /// Gets or sets the estado.
        /// </summary>
        /// <value>
        /// The estado.
        /// </value>
        [DataMember(Name = "Estado_Tarefa")]
        public EstadoTarefa Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        /// <summary>
        /// Gets or sets the identifier projecto.
        /// </summary>
        /// <value>
        /// The identifier projecto.
        /// </value>
        [DataMember(Name = "IdProjecto_Tarefa")]
        public Guid IdProjecto
        {
            get { return idProjecto; }
            set { idProjecto = value; }
        }

        [DataMember(Name = "Inicio_Tarefa")]
        public DateTime Inicio
        {
            get { return inicio; }
            set { inicio = value; }
        }

        [DataMember(Name = "Fim_Tarefa")]
        public DateTime Fim
        {
            get { return fim; }
            set { fim = value; }
        }

        #endregion

        #region Métodos

        public override bool Equals(object obj)
        {
            Tarefa t = obj as Tarefa;
            return t.Id == Id;
        }

        #endregion



    }
}