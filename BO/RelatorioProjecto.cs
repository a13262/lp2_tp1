﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System.Collections.Generic;

namespace BO
{
    public class RelatorioProjecto
    {
        #region Estado

        private string nome;
        private string nomeResponsavel;
        private string tipo;
        private List<Tarefa> tarefasTerminadas = new List<Tarefa>();
        private List<Tarefa> tarefasEmAtraso = new List<Tarefa>();
        private List<Recurso> recursos = new List<Recurso>();

        #endregion

        #region Constructor

        public RelatorioProjecto()
        {
        }

        #endregion

        #region Propriedades
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string NomeResponsavel
        {
            get { return nomeResponsavel; }
            set { nomeResponsavel = value; }
        }

        public List<Tarefa> TarefasTerminadas
        {
            get { return tarefasTerminadas; }
            set { tarefasTerminadas = value; }
        }

        public List<Tarefa> TarefasEmAtraso
        {
            get { return tarefasEmAtraso; }
            set { tarefasEmAtraso = value; }
        }

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public List<Recurso> Recursos
        {
            get { return recursos; }
            set { recursos = value; }
        }

        #endregion

        #region Métodos

        #endregion



    }
}