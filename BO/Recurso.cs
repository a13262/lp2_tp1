﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Runtime.Serialization;

namespace BO
{
    /// <summary>
    /// Representa um recurso
    /// </summary>
    [DataContract(Name = "Recurso")]
    public class Recurso
    {
        #region Estado

        /// <summary>
        /// The identifier
        /// </summary>
        private Guid id;

        /// <summary>
        /// The descricao
        /// </summary>
        private string descricao;

        private Guid idElementoAssociado;

        
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Recurso" /> class.
        /// </summary>
        public Recurso(string descricao, Guid idElementoAssociado)
        {
            Id = Guid.NewGuid();
            Descricao = descricao;
            IdElementoAssociado = idElementoAssociado;

        }

        public Recurso(Guid id, string descricao, Guid idElementoAssociado)
        {
            Id = id;
            Descricao = descricao;
            IdElementoAssociado = idElementoAssociado;
        }

        public Recurso(Guid id, Guid idElementoAssociado)
        {
            Id = id;
            IdElementoAssociado = idElementoAssociado;
        }

        public Recurso()
        {
            Id = Guid.NewGuid();
        }

        #endregion

        #region Propriedades
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember(Name = "Id_Recurso")]
        public System.Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets or sets the descricao.
        /// </summary>
        /// <value>
        /// The descricao.
        /// </value>
        [DataMember(Name = "Descricao_Recurso")]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        [DataMember(Name = "IdElementoAssociado_Recurso")]
        public Guid IdElementoAssociado
        {
            get { return idElementoAssociado; }
            set { idElementoAssociado = value; }
        }

        #endregion

        #region Métodos

        public override bool Equals(object obj)
        {
            if (obj is Recurso r) return r.Id == Id;

            return false;
        }

        #endregion



    }
}