﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;

namespace Common
{
    public interface ICrud<T>
    {
        bool Create(T obj);
        T Read(System.Guid id);
        List<T> Read();
        bool Update(T obj);
        bool Delete(System.Guid id);
        }
}