﻿namespace PL
{
    partial class Recursos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.recursoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.novo = new System.Windows.Forms.Button();
            this.tabelaRecursos = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdElementoAssociado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelRecursos = new System.Windows.Forms.Label();
            this.tabelaRecursosProjecto = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idElementoAssociadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recursoProjectoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelRecursos2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.recursoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaRecursos)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaRecursosProjecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recursoProjectoBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // recursoBindingSource
            // 
            this.recursoBindingSource.DataSource = typeof(BO.Recurso);
            this.recursoBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.OnBindingSource_ListChanged);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.novo);
            this.splitContainer.Panel1.Controls.Add(this.tabelaRecursos);
            this.splitContainer.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.tabelaRecursosProjecto);
            this.splitContainer.Panel2.Controls.Add(this.panel2);
            this.splitContainer.Size = new System.Drawing.Size(800, 749);
            this.splitContainer.SplitterDistance = 374;
            this.splitContainer.TabIndex = 3;
            // 
            // novo
            // 
            this.novo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.novo.Location = new System.Drawing.Point(0, 298);
            this.novo.Name = "novo";
            this.novo.Size = new System.Drawing.Size(800, 76);
            this.novo.TabIndex = 2;
            this.novo.Text = "Novo";
            this.novo.UseVisualStyleBackColor = true;
            this.novo.Click += new System.EventHandler(this.OnNovoClick);
            // 
            // tabelaRecursos
            // 
            this.tabelaRecursos.AllowUserToAddRows = false;
            this.tabelaRecursos.AllowUserToOrderColumns = true;
            this.tabelaRecursos.AutoGenerateColumns = false;
            this.tabelaRecursos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaRecursos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.descricaoDataGridViewTextBoxColumn,
            this.IdElementoAssociado});
            this.tabelaRecursos.DataSource = this.recursoBindingSource;
            this.tabelaRecursos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaRecursos.Location = new System.Drawing.Point(0, 27);
            this.tabelaRecursos.Name = "tabelaRecursos";
            this.tabelaRecursos.Size = new System.Drawing.Size(800, 347);
            this.tabelaRecursos.TabIndex = 3;
            this.tabelaRecursos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellDoubleClick);
            this.tabelaRecursos.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnUserDeletingRow);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // descricaoDataGridViewTextBoxColumn
            // 
            this.descricaoDataGridViewTextBoxColumn.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.Name = "descricaoDataGridViewTextBoxColumn";
            // 
            // IdElementoAssociado
            // 
            this.IdElementoAssociado.DataPropertyName = "IdElementoAssociado";
            this.IdElementoAssociado.HeaderText = "IdElementoAssociado";
            this.IdElementoAssociado.Name = "IdElementoAssociado";
            this.IdElementoAssociado.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelRecursos);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 27);
            this.panel1.TabIndex = 4;
            // 
            // labelRecursos
            // 
            this.labelRecursos.AutoSize = true;
            this.labelRecursos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecursos.Location = new System.Drawing.Point(329, 4);
            this.labelRecursos.Name = "labelRecursos";
            this.labelRecursos.Size = new System.Drawing.Size(161, 20);
            this.labelRecursos.TabIndex = 0;
            this.labelRecursos.Text = "Recursos Disponíveis";
            // 
            // tabelaRecursosProjecto
            // 
            this.tabelaRecursosProjecto.AllowUserToAddRows = false;
            this.tabelaRecursosProjecto.AllowUserToDeleteRows = false;
            this.tabelaRecursosProjecto.AutoGenerateColumns = false;
            this.tabelaRecursosProjecto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaRecursosProjecto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.descricaoDataGridViewTextBoxColumn1,
            this.idElementoAssociadoDataGridViewTextBoxColumn});
            this.tabelaRecursosProjecto.DataSource = this.recursoProjectoBindingSource;
            this.tabelaRecursosProjecto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaRecursosProjecto.Location = new System.Drawing.Point(0, 27);
            this.tabelaRecursosProjecto.Name = "tabelaRecursosProjecto";
            this.tabelaRecursosProjecto.ReadOnly = true;
            this.tabelaRecursosProjecto.Size = new System.Drawing.Size(800, 344);
            this.tabelaRecursosProjecto.TabIndex = 6;
            this.tabelaRecursosProjecto.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tabelaRecursosProjecto_CellDoubleClick);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // descricaoDataGridViewTextBoxColumn1
            // 
            this.descricaoDataGridViewTextBoxColumn1.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn1.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn1.Name = "descricaoDataGridViewTextBoxColumn1";
            this.descricaoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // idElementoAssociadoDataGridViewTextBoxColumn
            // 
            this.idElementoAssociadoDataGridViewTextBoxColumn.DataPropertyName = "IdElementoAssociado";
            this.idElementoAssociadoDataGridViewTextBoxColumn.HeaderText = "IdElementoAssociado";
            this.idElementoAssociadoDataGridViewTextBoxColumn.Name = "idElementoAssociadoDataGridViewTextBoxColumn";
            this.idElementoAssociadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // recursoProjectoBindingSource
            // 
            this.recursoProjectoBindingSource.DataSource = typeof(BO.Recurso);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelRecursos2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 27);
            this.panel2.TabIndex = 5;
            // 
            // labelRecursos2
            // 
            this.labelRecursos2.AutoSize = true;
            this.labelRecursos2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecursos2.Location = new System.Drawing.Point(328, 3);
            this.labelRecursos2.Name = "labelRecursos2";
            this.labelRecursos2.Size = new System.Drawing.Size(147, 20);
            this.labelRecursos2.TabIndex = 1;
            this.labelRecursos2.Text = "Recursos Alocados";
            // 
            // Recursos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 749);
            this.Controls.Add(this.splitContainer);
            this.Name = "Recursos";
            this.Text = "Recursos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnClose);
            this.Load += new System.EventHandler(this.OnInit);
            ((System.ComponentModel.ISupportInitialize)(this.recursoBindingSource)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaRecursos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaRecursosProjecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recursoProjectoBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource recursoBindingSource;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button novo;
        private System.Windows.Forms.DataGridView tabelaRecursos;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdElementoAssociado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelRecursos;
        private System.Windows.Forms.DataGridView tabelaRecursosProjecto;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelRecursos2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idElementoAssociadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource recursoProjectoBindingSource;
    }
}