﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BO;

namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Tarefas : Form
    {
        /// <summary>
        /// The data binding source loaded
        /// </summary>
        private bool dataBindingSourceLoaded = false;
        /// <summary>
        /// The identifier projecto
        /// </summary>
        private Guid idProjecto;
        /// <summary>
        /// The identifier tarefa pai
        /// </summary>
        private Guid idTarefaPai;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefas"/> class.
        /// </summary>
        /// <param name="idProjecto">The identifier projecto.</param>
        public Tarefas(Guid idProjecto)
        {
            InitializeComponent();
            estadoDataGridViewTextBoxColumn.DataSource = Enum.GetValues(typeof(Tarefa.EstadoTarefa));
            IdProjecto = idProjecto;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Tarefas"/> class.
        /// </summary>
        /// <param name="idProjecto">The identifier projecto.</param>
        /// <param name="idTarefaPai">The identifier tarefa pai.</param>
        public Tarefas(Guid idProjecto, Guid idTarefaPai)
        {
            InitializeComponent();
            estadoDataGridViewTextBoxColumn.DataSource = Enum.GetValues(typeof(Tarefa.EstadoTarefa));
            IdProjecto = idProjecto;
            IdTarefaPai = idTarefaPai;
        }

        /// <summary>
        /// Gets or sets the identifier projecto.
        /// </summary>
        /// <value>
        /// The identifier projecto.
        /// </value>
        public Guid IdProjecto
        {
            get { return idProjecto; }
            set { idProjecto = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [data binding source loaded].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [data binding source loaded]; otherwise, <c>false</c>.
        /// </value>
        public bool DataBindingSourceLoaded
        {
            get { return dataBindingSourceLoaded; }
            set { dataBindingSourceLoaded = value; }
        }

        /// <summary>
        /// Gets or sets the identifier tarefa pai.
        /// </summary>
        /// <value>
        /// The identifier tarefa pai.
        /// </value>
        public Guid IdTarefaPai
        {
            get { return idTarefaPai; }
            set { idTarefaPai = value; }
        }

        /// <summary>
        /// Called when [novo click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnNovoClick(object sender, EventArgs e)
        {
            tarefaBindingSource.Add(new Tarefa());
        }

        /// <summary>
        /// Called when [binding source list changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ListChangedEventArgs"/> instance containing the event data.</param>
        private void OnBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                if (DataBindingSourceLoaded)
                {
                    var tpl = new TarefaPL(IdProjecto);

                    if (e.ListChangedType == ListChangedType.ItemChanged)
                    {
                        Tarefa t = tarefaBindingSource[e.NewIndex] as Tarefa;
                        if (tpl.Update(t))
                        {
                            MessageBox.Show("Registo actualizado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao actualizar o registo!");
                        }

                    }
                    else if (e.ListChangedType == ListChangedType.ItemAdded)
                    {
                        Tarefa t = tarefaBindingSource[e.NewIndex] as Tarefa;
                        if (tpl.Create(t))
                        {
                            if (dataBindingSourceLoaded) MessageBox.Show("Registo criado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao criar o registo!");
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
        }

        /// <summary>
        /// Called when [user deleting row].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataGridViewRowCancelEventArgs"/> instance containing the event data.</param>
        private void OnUserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                Tarefa t = e.Row.DataBoundItem as Tarefa;
                var tpl = new TarefaPL(IdProjecto);
                if (tpl.Delete(t.Id))
                {
                    MessageBox.Show("Registo apagado com sucesso!");
                }
                else
                {
                    MessageBox.Show("Falha ao apagar o registo!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
        }

        /// <summary>
        /// Called when [initialize].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnInit(object sender, EventArgs e)
        {
            inicioTarefa.Visible = false;
            fimTarefa.Visible = false;

            try
            {
                var tpl = new TarefaPL(IdProjecto);
                foreach (Tarefa t in tpl.Read())
                {
                    tarefaBindingSource.Add(t);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            finally
            {
                DataBindingSourceLoaded = true;
                tarefaBindingSource.ResetBindings(false);
            }
        }

        /// <summary>
        /// Called when [cell click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void OnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        if (this.Owner == null) return;
                        if (tarefaBindingSource.Current is Tarefa t3)
                        {
                            IdTarefaPai = t3.Id;
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                        }
                        break;
                    case 5:
                        if (tarefaBindingSource.Current is Tarefa t)
                        {
                            var recursos = new Recursos(t.Id);
                            recursos.Owner = this;
                            var result = recursos.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, tarefaBindingSource.IndexOf(t)));

                            }
                        }
                        break;
                    case 3:
                        if (tarefaBindingSource.Current is Tarefa t2)
                        {
                            var tarefas = new Tarefas(this.IdProjecto, t2.Id);
                            tarefas.Owner = this;
                            tarefas.tabelaTarefas.AllowUserToAddRows = false;
                            tarefas.tabelaTarefas.AllowUserToDeleteRows = false;
                            tarefas.tabelaTarefas.ReadOnly = true;
                            var result = tarefas.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                if (t2.Id == tarefas.IdTarefaPai)
                                {
                                    t2.IdTarefaPai = Guid.Empty;
                                }
                                else
                                {
                                    t2.IdTarefaPai = tarefas.IdTarefaPai;
                                }

                                OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, tarefaBindingSource.IndexOf(t2)));
                            }
                        }
                        break;
                    case 6:
                        inicioTarefa.Visible = true;
                        inicioTarefa.Select();
                        SendKeys.Send("%{DOWN}");
                        break;
                    case 7:
                        fimTarefa.Visible = true;
                        fimTarefa.Select();
                        SendKeys.Send("%{DOWN}");
                        break;

                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            
        }

        /// <summary>
        /// Handles the CloseUp event of the inicioTarefa control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void inicioTarefa_CloseUp(object sender, EventArgs e)
        {
            try
            {
                if (tarefaBindingSource.Current is Tarefa t)
                {
                    t.Inicio = inicioTarefa.Value;
                    OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, tarefaBindingSource.IndexOf(t)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }finally { inicioTarefa.Visible = false; }
            
        }

        /// <summary>
        /// Handles the CloseUp event of the fimTarefa control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void fimTarefa_CloseUp(object sender, EventArgs e)
        {
            try
            {
                if (tarefaBindingSource.Current is Tarefa t)
                {
                    t.Fim = fimTarefa.Value;
                    OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, tarefaBindingSource.IndexOf(t)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }finally { fimTarefa.Visible = false; }

        }
    }
}
