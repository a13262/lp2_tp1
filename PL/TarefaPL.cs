﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using BLL;
using BO;
using Common;

namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Tarefa}" />
    public class TarefaPL:ICrud<Tarefa>
    {
        #region Estado

        /// <summary>
        /// The identifier projecto
        /// </summary>
        private Guid idProjecto;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TarefaPL"/> class.
        /// </summary>
        /// <param name="idProjecto">The identifier projecto.</param>
        public TarefaPL(Guid idProjecto)
        {
            IdProjecto = idProjecto;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TarefaPL"/> class.
        /// </summary>
        public TarefaPL()
        {

        }


        #endregion

        #region Propriedades
        /// <summary>
        /// Gets or sets the identifier projecto.
        /// </summary>
        /// <value>
        /// The identifier projecto.
        /// </value>
        public Guid IdProjecto
        {
            get { return idProjecto; }
            set { idProjecto = value; }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Tarefa obj)
        {
            try
            {
                var tbll = new TarefaBLL(IdProjecto);
                return tbll.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Tarefa Read(Guid id)
        {
            try
            {
                var tbll = new TarefaBLL();
                return tbll.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Tarefa> Read()
        {
            try
            {
                var tbll = new TarefaBLL(IdProjecto);
                return tbll.Read();
            }
            catch (Exception e)
            {
                throw;
            }
            
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Tarefa obj)
        {
            try
            {
                var tbll = new TarefaBLL(IdProjecto);
                return tbll.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            try
            {
                var tbll = new TarefaBLL();
                return tbll.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        #endregion



    }
}