﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BO;


namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Pessoas : Form
    {
        /// <summary>
        /// The data binding source loaded
        /// </summary>
        private bool dataBindingSourceLoaded = false;
        /// <summary>
        /// The identifier responsavel
        /// </summary>
        private Guid idResponsavel;
        /// <summary>
        /// The identifier elemento assoc
        /// </summary>
        private Guid idElementoAssoc;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pessoas"/> class.
        /// </summary>
        public Pessoas()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pessoas"/> class.
        /// </summary>
        /// <param name="idResponsavel">The identifier responsavel.</param>
        /// <param name="idElementoAssoc">The identifier elemento assoc.</param>
        public Pessoas(Guid idResponsavel, Guid idElementoAssoc)
        {
            InitializeComponent();
            IdResponsavel = idResponsavel;
            IdElementoAssoc = idElementoAssoc;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [data binding source loaded].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [data binding source loaded]; otherwise, <c>false</c>.
        /// </value>
        public bool DataBindingSourceLoaded
        {
            get { return dataBindingSourceLoaded; }
            set { dataBindingSourceLoaded = value; }
        }

        /// <summary>
        /// Gets or sets the identifier responsavel.
        /// </summary>
        /// <value>
        /// The identifier responsavel.
        /// </value>
        public Guid IdResponsavel
        {
            get { return idResponsavel; }
            set { idResponsavel = value; }
        }

        /// <summary>
        /// Gets or sets the identifier elemento assoc.
        /// </summary>
        /// <value>
        /// The identifier elemento assoc.
        /// </value>
        public Guid IdElementoAssoc
        {
            get { return idElementoAssoc; }
            set { idElementoAssoc = value; }
        }

        /// <summary>
        /// Called when [initialize].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnInit(object sender, EventArgs e)
        {
            try
            {
                var rbll = new PessoaPL();
                foreach (Pessoa r in rbll.Read())
                {
                    pessoaBindingSource.Add(r);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            finally
            {
                DataBindingSourceLoaded = true;
                pessoaBindingSource.ResetBindings(false);
                if (this.Owner!=null)
                {
                    int index = pessoaBindingSource.IndexOf(new Pessoa(IdResponsavel));
                    if(index >= 0)
                    tabelaPessoas.Rows[index].Selected = true;

                }
            }
        }

        /// <summary>
        /// Called when [binding source list changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ListChangedEventArgs"/> instance containing the event data.</param>
        private void OnBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                if (DataBindingSourceLoaded)
                {
                    var ppl = new PessoaPL();

                    if (e.ListChangedType == ListChangedType.ItemChanged)
                    {
                        Pessoa p = pessoaBindingSource[e.NewIndex] as Pessoa;
                        if (ppl.Update(p))
                        {
                            MessageBox.Show("Registo actualizado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao actualizar o registo!");
                        }

                    }
                    else if (e.ListChangedType == ListChangedType.ItemAdded)
                    {
                        Pessoa p = pessoaBindingSource[e.NewIndex] as Pessoa;
                        if (ppl.Create(p))
                        {
                            if (dataBindingSourceLoaded) MessageBox.Show("Registo criado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao criar o registo!");
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
        }

        /// <summary>
        /// Called when [user deleting row].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataGridViewRowCancelEventArgs"/> instance containing the event data.</param>
        private void OnUserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                Pessoa p = e.Row.DataBoundItem as Pessoa;
                var ppl = new PessoaPL();
                if (ppl.Delete(p.Id))
                {
                    MessageBox.Show("Registo apagado com sucesso!");
                }
                else
                {
                    MessageBox.Show("Falha ao apagar o registo!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
        }

        /// <summary>
        /// Called when [cell double click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void OnCellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.Owner == null) return;
                switch (e.ColumnIndex)
                {
                    case 0:
                        if (pessoaBindingSource.Current is Pessoa p)
                        {
                            IdResponsavel = p.Id;
                            p.IdElementoAssociado = IdElementoAssoc;
                            OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, pessoaBindingSource.IndexOf(p)));
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }

        }

        /// <summary>
        /// Called when [novo click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnNovoClick(object sender, EventArgs e)
        {
            pessoaBindingSource.Add(new Pessoa());
        }
    }
}
