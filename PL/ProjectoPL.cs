﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using BO;
using Common;
using BLL;
namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Projecto}" />
    public class ProjectoPL:ICrud<Projecto>
    {
        #region Estado

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectoPL"/> class.
        /// </summary>
        public ProjectoPL()
        {
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Projecto obj)
        {
            try
            {
                var pbll = new ProjectoBLL();
                return pbll.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Projecto Read(Guid id)
        {
            try
            {
                var pbll = new ProjectoBLL();
                return pbll.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Projecto> Read()
        {
            try
            {
                var pbll = new ProjectoBLL();
                return pbll.Read();
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Projecto obj)
        {
            try
            {
                var pbll = new ProjectoBLL();
                return pbll.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            try
            {
                var pbll = new ProjectoBLL();
                return pbll.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Relatorioes the projecto.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static RelatorioProjecto RelatorioProjecto(Guid id)
        {
            try
            {
                return ProjectoBLL.RelatorioProjecto(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        #endregion



    }
}