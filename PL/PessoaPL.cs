﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using BO;
using Common;
using BLL;

namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Pessoa}" />
    public class PessoaPL:ICrud<Pessoa>
    {
        #region Estado

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PessoaPL"/> class.
        /// </summary>
        public PessoaPL()
        {
        }

        #endregion

        #region Propriedades

        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Pessoa obj)
        {
            try
            {
                var pbll = new PessoaBLL();
                return pbll.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Pessoa Read(Guid id)
        {
            try
            {
                var pbll = new PessoaBLL();
                return pbll.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Pessoa> Read()
        {
            try
            {
                var pbll = new PessoaBLL();
                return pbll.Read();
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Pessoa obj)
        {
            try
            {
                var pbll = new PessoaBLL();
                return pbll.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            try
            {
                var pbll = new PessoaBLL();
                return pbll.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }
        #endregion

    }
}