﻿using BO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Relatorio : Form
    {
        /// <summary>
        /// The identifier projecto
        /// </summary>
        private Guid idProjecto;

        /// <summary>
        /// Initializes a new instance of the <see cref="Relatorio"/> class.
        /// </summary>
        /// <param name="idProjecto">The identifier projecto.</param>
        public Relatorio(Guid idProjecto)
        {
            InitializeComponent();
            IdProjecto = idProjecto;
        }

        /// <summary>
        /// Gets or sets the identifier projecto.
        /// </summary>
        /// <value>
        /// The identifier projecto.
        /// </value>
        public Guid IdProjecto
        {
            get { return idProjecto; }
            set { idProjecto = value; }
        }

        /// <summary>
        /// Called when [initialize].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnInit(object sender, EventArgs e)
        {
            if(IdProjecto == Guid.Empty) return;
            try
            {
                RelatorioProjecto relatorio = ProjectoPL.RelatorioProjecto(IdProjecto);

                if(relatorio == null) return;

                nomeProjecto.Text = relatorio.Nome;
                responsavelProjecto.Text = relatorio.NomeResponsavel;
                tipoProjecto.Text = relatorio.Tipo;

                foreach (Tarefa t in relatorio.TarefasTerminadas)
                {
                    tarefasTerminadasBindingSource.Add(t);
                }
                foreach (Tarefa t in relatorio.TarefasEmAtraso)
                {
                    tarefasEmAtraso.Add(t);
                }

                foreach (Recurso r in relatorio.Recursos)
                {
                    recursoBindingSource.Add(r);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
        }
    }
}
