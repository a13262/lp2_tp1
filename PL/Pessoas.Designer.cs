﻿namespace PL
{
    partial class Pessoas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabelaPessoas = new System.Windows.Forms.DataGridView();
            this.pessoaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.novo = new System.Windows.Forms.Button();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdElementoAssociado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaPessoas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pessoaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabelaPessoas
            // 
            this.tabelaPessoas.AllowUserToAddRows = false;
            this.tabelaPessoas.AutoGenerateColumns = false;
            this.tabelaPessoas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaPessoas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nomeDataGridViewTextBoxColumn,
            this.descricaoDataGridViewTextBoxColumn,
            this.IdElementoAssociado});
            this.tabelaPessoas.DataSource = this.pessoaBindingSource;
            this.tabelaPessoas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaPessoas.Location = new System.Drawing.Point(0, 0);
            this.tabelaPessoas.Name = "tabelaPessoas";
            this.tabelaPessoas.Size = new System.Drawing.Size(800, 450);
            this.tabelaPessoas.TabIndex = 0;
            this.tabelaPessoas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellDoubleClick);
            this.tabelaPessoas.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnUserDeletingRow);
            // 
            // pessoaBindingSource
            // 
            this.pessoaBindingSource.DataSource = typeof(BO.Pessoa);
            this.pessoaBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.OnBindingSource_ListChanged);
            // 
            // novo
            // 
            this.novo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.novo.Location = new System.Drawing.Point(0, 373);
            this.novo.Name = "novo";
            this.novo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.novo.Size = new System.Drawing.Size(800, 77);
            this.novo.TabIndex = 1;
            this.novo.Text = "Novo";
            this.novo.UseVisualStyleBackColor = true;
            this.novo.Click += new System.EventHandler(this.OnNovoClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomeDataGridViewTextBoxColumn
            // 
            this.nomeDataGridViewTextBoxColumn.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn.Name = "nomeDataGridViewTextBoxColumn";
            // 
            // descricaoDataGridViewTextBoxColumn
            // 
            this.descricaoDataGridViewTextBoxColumn.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.Name = "descricaoDataGridViewTextBoxColumn";
            // 
            // IdElementoAssociado
            // 
            this.IdElementoAssociado.DataPropertyName = "IdElementoAssociado";
            this.IdElementoAssociado.HeaderText = "IdElementoAssociado";
            this.IdElementoAssociado.Name = "IdElementoAssociado";
            // 
            // Pessoas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.novo);
            this.Controls.Add(this.tabelaPessoas);
            this.Name = "Pessoas";
            this.Text = "Pessoas";
            this.Load += new System.EventHandler(this.OnInit);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaPessoas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pessoaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tabelaPessoas;
        private System.Windows.Forms.BindingSource pessoaBindingSource;
        private System.Windows.Forms.Button novo;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdElementoAssociado;
    }
}