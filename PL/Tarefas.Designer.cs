﻿namespace PL
{
    partial class Tarefas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabelaTarefas = new System.Windows.Forms.DataGridView();
            this.novo = new System.Windows.Forms.Button();
            this.tarefaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProjectoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idTarefaPaiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Recursos = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Inicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inicioTarefa = new System.Windows.Forms.DateTimePicker();
            this.fimTarefa = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaTarefas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarefaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabelaTarefas
            // 
            this.tabelaTarefas.AllowUserToAddRows = false;
            this.tabelaTarefas.AutoGenerateColumns = false;
            this.tabelaTarefas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaTarefas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.idProjectoDataGridViewTextBoxColumn,
            this.descricaoDataGridViewTextBoxColumn,
            this.idTarefaPaiDataGridViewTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn,
            this.Recursos,
            this.Inicio,
            this.Fim});
            this.tabelaTarefas.DataSource = this.tarefaBindingSource;
            this.tabelaTarefas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaTarefas.Location = new System.Drawing.Point(0, 0);
            this.tabelaTarefas.Name = "tabelaTarefas";
            this.tabelaTarefas.Size = new System.Drawing.Size(846, 425);
            this.tabelaTarefas.TabIndex = 0;
            this.tabelaTarefas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellClick);
            this.tabelaTarefas.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnUserDeletingRow);
            // 
            // novo
            // 
            this.novo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.novo.Location = new System.Drawing.Point(0, 345);
            this.novo.Name = "novo";
            this.novo.Size = new System.Drawing.Size(846, 80);
            this.novo.TabIndex = 1;
            this.novo.Text = "Novo";
            this.novo.UseVisualStyleBackColor = true;
            this.novo.Click += new System.EventHandler(this.OnNovoClick);
            // 
            // tarefaBindingSource
            // 
            this.tarefaBindingSource.DataSource = typeof(BO.Tarefa);
            this.tarefaBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.OnBindingSource_ListChanged);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idProjectoDataGridViewTextBoxColumn
            // 
            this.idProjectoDataGridViewTextBoxColumn.DataPropertyName = "IdProjecto";
            this.idProjectoDataGridViewTextBoxColumn.HeaderText = "IdProjecto";
            this.idProjectoDataGridViewTextBoxColumn.Name = "idProjectoDataGridViewTextBoxColumn";
            // 
            // descricaoDataGridViewTextBoxColumn
            // 
            this.descricaoDataGridViewTextBoxColumn.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.Name = "descricaoDataGridViewTextBoxColumn";
            // 
            // idTarefaPaiDataGridViewTextBoxColumn
            // 
            this.idTarefaPaiDataGridViewTextBoxColumn.DataPropertyName = "IdTarefaPai";
            this.idTarefaPaiDataGridViewTextBoxColumn.HeaderText = "IdTarefaPai";
            this.idTarefaPaiDataGridViewTextBoxColumn.Name = "idTarefaPaiDataGridViewTextBoxColumn";
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.estadoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Recursos
            // 
            this.Recursos.HeaderText = "Recursos";
            this.Recursos.Name = "Recursos";
            this.Recursos.Text = "Ver";
            this.Recursos.ToolTipText = "Recursos";
            this.Recursos.UseColumnTextForButtonValue = true;
            // 
            // Inicio
            // 
            this.Inicio.DataPropertyName = "Inicio";
            this.Inicio.HeaderText = "Inicio";
            this.Inicio.Name = "Inicio";
            // 
            // Fim
            // 
            this.Fim.DataPropertyName = "Fim";
            this.Fim.HeaderText = "Fim";
            this.Fim.Name = "Fim";
            // 
            // inicioTarefa
            // 
            this.inicioTarefa.Location = new System.Drawing.Point(634, 0);
            this.inicioTarefa.Name = "inicioTarefa";
            this.inicioTarefa.Size = new System.Drawing.Size(108, 20);
            this.inicioTarefa.TabIndex = 2;
            this.inicioTarefa.CloseUp += new System.EventHandler(this.inicioTarefa_CloseUp);
            // 
            // fimTarefa
            // 
            this.fimTarefa.Location = new System.Drawing.Point(739, 0);
            this.fimTarefa.Name = "fimTarefa";
            this.fimTarefa.Size = new System.Drawing.Size(106, 20);
            this.fimTarefa.TabIndex = 3;
            this.fimTarefa.CloseUp += new System.EventHandler(this.fimTarefa_CloseUp);
            // 
            // Tarefas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 425);
            this.Controls.Add(this.fimTarefa);
            this.Controls.Add(this.inicioTarefa);
            this.Controls.Add(this.novo);
            this.Controls.Add(this.tabelaTarefas);
            this.Name = "Tarefas";
            this.Text = "Tarefas";
            this.Load += new System.EventHandler(this.OnInit);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaTarefas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarefaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tabelaTarefas;
        private System.Windows.Forms.BindingSource tarefaBindingSource;
        private System.Windows.Forms.Button novo;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProjectoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idTarefaPaiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn Recursos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fim;
        private System.Windows.Forms.DateTimePicker inicioTarefa;
        private System.Windows.Forms.DateTimePicker fimTarefa;
    }
}