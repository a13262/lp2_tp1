﻿namespace PL
{
    partial class Projectos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabelaProjectos = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inicioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fimDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Responsavel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Tarefas = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Recursos = new System.Windows.Forms.DataGridViewButtonColumn();
            this.RelatorioProjecto = new System.Windows.Forms.DataGridViewButtonColumn();
            this.projectoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.inicio = new System.Windows.Forms.DateTimePicker();
            this.fim = new System.Windows.Forms.DateTimePicker();
            this.novo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaProjectos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabelaProjectos
            // 
            this.tabelaProjectos.AllowUserToAddRows = false;
            this.tabelaProjectos.AutoGenerateColumns = false;
            this.tabelaProjectos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaProjectos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nomeDataGridViewTextBoxColumn,
            this.inicioDataGridViewTextBoxColumn,
            this.fimDataGridViewTextBoxColumn,
            this.tipoDataGridViewTextBoxColumn,
            this.Responsavel,
            this.Tarefas,
            this.Recursos,
            this.RelatorioProjecto});
            this.tabelaProjectos.DataSource = this.projectoBindingSource;
            this.tabelaProjectos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaProjectos.Location = new System.Drawing.Point(0, 0);
            this.tabelaProjectos.Name = "tabelaProjectos";
            this.tabelaProjectos.Size = new System.Drawing.Size(944, 450);
            this.tabelaProjectos.TabIndex = 0;
            this.tabelaProjectos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellClick);
            this.tabelaProjectos.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.tabelaProjectos_UserDeletingRow);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomeDataGridViewTextBoxColumn
            // 
            this.nomeDataGridViewTextBoxColumn.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn.Name = "nomeDataGridViewTextBoxColumn";
            // 
            // inicioDataGridViewTextBoxColumn
            // 
            this.inicioDataGridViewTextBoxColumn.DataPropertyName = "Inicio";
            this.inicioDataGridViewTextBoxColumn.HeaderText = "Inicio";
            this.inicioDataGridViewTextBoxColumn.Name = "inicioDataGridViewTextBoxColumn";
            // 
            // fimDataGridViewTextBoxColumn
            // 
            this.fimDataGridViewTextBoxColumn.DataPropertyName = "Fim";
            this.fimDataGridViewTextBoxColumn.HeaderText = "Fim";
            this.fimDataGridViewTextBoxColumn.Name = "fimDataGridViewTextBoxColumn";
            // 
            // tipoDataGridViewTextBoxColumn
            // 
            this.tipoDataGridViewTextBoxColumn.DataPropertyName = "Tipo";
            this.tipoDataGridViewTextBoxColumn.HeaderText = "Tipo";
            this.tipoDataGridViewTextBoxColumn.Name = "tipoDataGridViewTextBoxColumn";
            this.tipoDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tipoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Responsavel
            // 
            this.Responsavel.DataPropertyName = "Responsavel";
            this.Responsavel.HeaderText = "Responsável";
            this.Responsavel.Name = "Responsavel";
            this.Responsavel.Text = "Ver";
            this.Responsavel.ToolTipText = "Responsável";
            this.Responsavel.UseColumnTextForButtonValue = true;
            // 
            // Tarefas
            // 
            this.Tarefas.DataPropertyName = "Tarefas";
            this.Tarefas.HeaderText = "Tarefas";
            this.Tarefas.Name = "Tarefas";
            this.Tarefas.Text = "Ver";
            this.Tarefas.ToolTipText = "Tarefas";
            this.Tarefas.UseColumnTextForButtonValue = true;
            // 
            // Recursos
            // 
            this.Recursos.DataPropertyName = "Recursos";
            this.Recursos.HeaderText = "Recursos";
            this.Recursos.Name = "Recursos";
            this.Recursos.Text = "Ver";
            this.Recursos.ToolTipText = "Recursos";
            this.Recursos.UseColumnTextForButtonValue = true;
            // 
            // RelatorioProjecto
            // 
            this.RelatorioProjecto.HeaderText = "Relatório";
            this.RelatorioProjecto.Name = "RelatorioProjecto";
            this.RelatorioProjecto.Text = "Ver";
            this.RelatorioProjecto.ToolTipText = "Relatório";
            this.RelatorioProjecto.UseColumnTextForButtonValue = true;
            // 
            // projectoBindingSource
            // 
            this.projectoBindingSource.DataSource = typeof(BO.Projecto);
            this.projectoBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.OnBindingSource_ListChanged);
            // 
            // inicio
            // 
            this.inicio.CustomFormat = "";
            this.inicio.Location = new System.Drawing.Point(242, 1);
            this.inicio.Name = "inicio";
            this.inicio.Size = new System.Drawing.Size(100, 20);
            this.inicio.TabIndex = 1;
            this.inicio.CloseUp += new System.EventHandler(this.OnInicioCloseUp);
            // 
            // fim
            // 
            this.fim.Location = new System.Drawing.Point(342, 1);
            this.fim.Name = "fim";
            this.fim.Size = new System.Drawing.Size(100, 20);
            this.fim.TabIndex = 2;
            this.fim.CloseUp += new System.EventHandler(this.OnFimCloseUp);
            // 
            // novo
            // 
            this.novo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.novo.Location = new System.Drawing.Point(0, 372);
            this.novo.Name = "novo";
            this.novo.Size = new System.Drawing.Size(944, 78);
            this.novo.TabIndex = 3;
            this.novo.Text = "Novo";
            this.novo.UseVisualStyleBackColor = true;
            this.novo.Click += new System.EventHandler(this.OnNovoClick);
            // 
            // Projectos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 450);
            this.Controls.Add(this.novo);
            this.Controls.Add(this.fim);
            this.Controls.Add(this.inicio);
            this.Controls.Add(this.tabelaProjectos);
            this.Name = "Projectos";
            this.Text = "Projectos";
            this.Load += new System.EventHandler(this.OnInit);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaProjectos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tabelaProjectos;
        private System.Windows.Forms.BindingSource projectoBindingSource;
        private System.Windows.Forms.DateTimePicker inicio;
        private System.Windows.Forms.DateTimePicker fim;
        private System.Windows.Forms.Button novo;
        private System.Windows.Forms.DataGridViewButtonColumn responsavelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inicioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fimDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn Responsavel;
        private System.Windows.Forms.DataGridViewButtonColumn Tarefas;
        private System.Windows.Forms.DataGridViewButtonColumn Recursos;
        private System.Windows.Forms.DataGridViewButtonColumn RelatorioProjecto;
    }
}