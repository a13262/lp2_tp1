﻿/*
	Autor: Tiago Rodrigues nº 13262
	E-mail: a13262@alunos.ipca.pt
*/

using System;
using System.Collections.Generic;
using BO;
using Common;
using BLL;
namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Common.ICrud{BO.Recurso}" />
    public class RecursoPL:ICrud<Recurso>
    {
        #region Estado

        /// <summary>
        /// The identifier elemento assoc
        /// </summary>
        private Guid idElementoAssoc;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RecursoPL"/> class.
        /// </summary>
        /// <param name="idElementoAssoc">The identifier elemento assoc.</param>
        public RecursoPL(Guid idElementoAssoc)
        {
            IdElementoAssoc = idElementoAssoc;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecursoPL"/> class.
        /// </summary>
        public RecursoPL()
        {
        }



        #endregion

        #region Propriedades
        /// <summary>
        /// Gets or sets the identifier elemento assoc.
        /// </summary>
        /// <value>
        /// The identifier elemento assoc.
        /// </value>
        public Guid IdElementoAssoc
        {
            get { return idElementoAssoc; }
            set { idElementoAssoc = value; }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Creates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Create(Recurso obj)
        {
            try
            {
                var rbll = new RecursoBLL();
                return rbll.Create(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Recurso Read(Guid id)
        {
            try
            {
                var rbll = new RecursoBLL();
                return rbll.Read(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns></returns>
        public List<Recurso> Read()
        {
            try
            {
                var rbll = new RecursoBLL(IdElementoAssoc);
                return rbll.Read();
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Reads the specified get all.
        /// </summary>
        /// <param name="getAll">if set to <c>true</c> [get all].</param>
        /// <returns></returns>
        public List<Recurso> Read(bool getAll)
        {
            try
            {
                var rbll = new RecursoBLL(IdElementoAssoc);
                return rbll.Read(getAll);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Update(Recurso obj)
        {
            try
            {
                var rbll = new RecursoBLL();
                return rbll.Update(obj);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            try
            {
                var rbll = new RecursoBLL();
                return rbll.Delete(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Frees the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Free(Guid id)
        {
            try
            {
                return RecursoBLL.Free(id);
            }
            catch (Exception e)
            {
                throw;
            }

        }

        #endregion



    }
}