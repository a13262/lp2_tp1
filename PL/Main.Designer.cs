﻿namespace PL
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.recursos = new System.Windows.Forms.Button();
            this.projectos = new System.Windows.Forms.Button();
            this.pessoas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // recursos
            // 
            this.recursos.Location = new System.Drawing.Point(170, 12);
            this.recursos.Name = "recursos";
            this.recursos.Size = new System.Drawing.Size(152, 72);
            this.recursos.TabIndex = 0;
            this.recursos.Text = "Recursos";
            this.recursos.UseVisualStyleBackColor = true;
            this.recursos.Click += new System.EventHandler(this.recursos_Click);
            // 
            // projectos
            // 
            this.projectos.Location = new System.Drawing.Point(12, 12);
            this.projectos.Name = "projectos";
            this.projectos.Size = new System.Drawing.Size(152, 72);
            this.projectos.TabIndex = 1;
            this.projectos.Text = "Projectos";
            this.projectos.UseVisualStyleBackColor = true;
            this.projectos.Click += new System.EventHandler(this.projectos_Click);
            // 
            // pessoas
            // 
            this.pessoas.Location = new System.Drawing.Point(328, 12);
            this.pessoas.Name = "pessoas";
            this.pessoas.Size = new System.Drawing.Size(152, 72);
            this.pessoas.TabIndex = 2;
            this.pessoas.Text = "Pessoas";
            this.pessoas.UseVisualStyleBackColor = true;
            this.pessoas.Click += new System.EventHandler(this.pessoas_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 284);
            this.Controls.Add(this.pessoas);
            this.Controls.Add(this.projectos);
            this.Controls.Add(this.recursos);
            this.Name = "Main";
            this.Text = "Main";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button recursos;
        private System.Windows.Forms.Button projectos;
        private System.Windows.Forms.Button pessoas;
    }
}

