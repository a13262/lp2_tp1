﻿namespace PL
{
    partial class Relatorio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelNomeProjecto = new System.Windows.Forms.Label();
            this.nomeProjecto = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabelaTarefasTerminadas = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idTarefaPaiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProjectoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarefasTerminadasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabelaTarefasEmAtraso = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idTarefaPaiDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProjectoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarefasEmAtraso = new System.Windows.Forms.BindingSource(this.components);
            this.labelTarefasTerminadas = new System.Windows.Forms.Label();
            this.labelTarefasEmAtraso = new System.Windows.Forms.Label();
            this.labelResponsavelProjecto = new System.Windows.Forms.Label();
            this.responsavelProjecto = new System.Windows.Forms.Label();
            this.labelTipoProjecto = new System.Windows.Forms.Label();
            this.tipoProjecto = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabelaRecursos = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idElementoAssociadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recursoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelRecursos = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaTarefasTerminadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarefasTerminadasBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaTarefasEmAtraso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarefasEmAtraso)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaRecursos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recursoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNomeProjecto
            // 
            this.labelNomeProjecto.AutoSize = true;
            this.labelNomeProjecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomeProjecto.Location = new System.Drawing.Point(12, 9);
            this.labelNomeProjecto.Name = "labelNomeProjecto";
            this.labelNomeProjecto.Size = new System.Drawing.Size(74, 25);
            this.labelNomeProjecto.TabIndex = 0;
            this.labelNomeProjecto.Text = "Nome:";
            // 
            // nomeProjecto
            // 
            this.nomeProjecto.AutoSize = true;
            this.nomeProjecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomeProjecto.Location = new System.Drawing.Point(92, 9);
            this.nomeProjecto.Name = "nomeProjecto";
            this.nomeProjecto.Size = new System.Drawing.Size(172, 25);
            this.nomeProjecto.TabIndex = 1;
            this.nomeProjecto.Text = "<nome projecto>";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabelaTarefasTerminadas);
            this.panel1.Location = new System.Drawing.Point(17, 126);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(775, 228);
            this.panel1.TabIndex = 2;
            // 
            // tabelaTarefasTerminadas
            // 
            this.tabelaTarefasTerminadas.AllowUserToAddRows = false;
            this.tabelaTarefasTerminadas.AllowUserToDeleteRows = false;
            this.tabelaTarefasTerminadas.AutoGenerateColumns = false;
            this.tabelaTarefasTerminadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaTarefasTerminadas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.descricaoDataGridViewTextBoxColumn,
            this.idTarefaPaiDataGridViewTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn,
            this.idProjectoDataGridViewTextBoxColumn,
            this.Inicio,
            this.Fim});
            this.tabelaTarefasTerminadas.DataSource = this.tarefasTerminadasBindingSource;
            this.tabelaTarefasTerminadas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaTarefasTerminadas.Location = new System.Drawing.Point(0, 0);
            this.tabelaTarefasTerminadas.Name = "tabelaTarefasTerminadas";
            this.tabelaTarefasTerminadas.ReadOnly = true;
            this.tabelaTarefasTerminadas.Size = new System.Drawing.Size(775, 228);
            this.tabelaTarefasTerminadas.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // descricaoDataGridViewTextBoxColumn
            // 
            this.descricaoDataGridViewTextBoxColumn.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.Name = "descricaoDataGridViewTextBoxColumn";
            this.descricaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idTarefaPaiDataGridViewTextBoxColumn
            // 
            this.idTarefaPaiDataGridViewTextBoxColumn.DataPropertyName = "IdTarefaPai";
            this.idTarefaPaiDataGridViewTextBoxColumn.HeaderText = "IdTarefaPai";
            this.idTarefaPaiDataGridViewTextBoxColumn.Name = "idTarefaPaiDataGridViewTextBoxColumn";
            this.idTarefaPaiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idProjectoDataGridViewTextBoxColumn
            // 
            this.idProjectoDataGridViewTextBoxColumn.DataPropertyName = "IdProjecto";
            this.idProjectoDataGridViewTextBoxColumn.HeaderText = "IdProjecto";
            this.idProjectoDataGridViewTextBoxColumn.Name = "idProjectoDataGridViewTextBoxColumn";
            this.idProjectoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Inicio
            // 
            this.Inicio.DataPropertyName = "Inicio";
            this.Inicio.HeaderText = "Inicio";
            this.Inicio.Name = "Inicio";
            this.Inicio.ReadOnly = true;
            // 
            // Fim
            // 
            this.Fim.DataPropertyName = "Fim";
            this.Fim.HeaderText = "Fim";
            this.Fim.Name = "Fim";
            this.Fim.ReadOnly = true;
            // 
            // tarefasTerminadasBindingSource
            // 
            this.tarefasTerminadasBindingSource.DataSource = typeof(BO.Tarefa);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabelaTarefasEmAtraso);
            this.panel2.Location = new System.Drawing.Point(17, 404);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(775, 228);
            this.panel2.TabIndex = 3;
            // 
            // tabelaTarefasEmAtraso
            // 
            this.tabelaTarefasEmAtraso.AllowUserToAddRows = false;
            this.tabelaTarefasEmAtraso.AllowUserToDeleteRows = false;
            this.tabelaTarefasEmAtraso.AutoGenerateColumns = false;
            this.tabelaTarefasEmAtraso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaTarefasEmAtraso.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.descricaoDataGridViewTextBoxColumn1,
            this.idTarefaPaiDataGridViewTextBoxColumn1,
            this.estadoDataGridViewTextBoxColumn1,
            this.idProjectoDataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.tabelaTarefasEmAtraso.DataSource = this.tarefasEmAtraso;
            this.tabelaTarefasEmAtraso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaTarefasEmAtraso.Location = new System.Drawing.Point(0, 0);
            this.tabelaTarefasEmAtraso.Name = "tabelaTarefasEmAtraso";
            this.tabelaTarefasEmAtraso.ReadOnly = true;
            this.tabelaTarefasEmAtraso.Size = new System.Drawing.Size(775, 228);
            this.tabelaTarefasEmAtraso.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // descricaoDataGridViewTextBoxColumn1
            // 
            this.descricaoDataGridViewTextBoxColumn1.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn1.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn1.Name = "descricaoDataGridViewTextBoxColumn1";
            this.descricaoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // idTarefaPaiDataGridViewTextBoxColumn1
            // 
            this.idTarefaPaiDataGridViewTextBoxColumn1.DataPropertyName = "IdTarefaPai";
            this.idTarefaPaiDataGridViewTextBoxColumn1.HeaderText = "IdTarefaPai";
            this.idTarefaPaiDataGridViewTextBoxColumn1.Name = "idTarefaPaiDataGridViewTextBoxColumn1";
            this.idTarefaPaiDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // estadoDataGridViewTextBoxColumn1
            // 
            this.estadoDataGridViewTextBoxColumn1.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn1.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn1.Name = "estadoDataGridViewTextBoxColumn1";
            this.estadoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // idProjectoDataGridViewTextBoxColumn1
            // 
            this.idProjectoDataGridViewTextBoxColumn1.DataPropertyName = "IdProjecto";
            this.idProjectoDataGridViewTextBoxColumn1.HeaderText = "IdProjecto";
            this.idProjectoDataGridViewTextBoxColumn1.Name = "idProjectoDataGridViewTextBoxColumn1";
            this.idProjectoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Inicio";
            this.dataGridViewTextBoxColumn1.HeaderText = "Inicio";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Fim";
            this.dataGridViewTextBoxColumn2.HeaderText = "Fim";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // tarefasEmAtraso
            // 
            this.tarefasEmAtraso.DataSource = typeof(BO.Tarefa);
            // 
            // labelTarefasTerminadas
            // 
            this.labelTarefasTerminadas.AutoSize = true;
            this.labelTarefasTerminadas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTarefasTerminadas.Location = new System.Drawing.Point(17, 103);
            this.labelTarefasTerminadas.Name = "labelTarefasTerminadas";
            this.labelTarefasTerminadas.Size = new System.Drawing.Size(150, 20);
            this.labelTarefasTerminadas.TabIndex = 0;
            this.labelTarefasTerminadas.Text = "Tarefas terminadas:";
            // 
            // labelTarefasEmAtraso
            // 
            this.labelTarefasEmAtraso.AutoSize = true;
            this.labelTarefasEmAtraso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTarefasEmAtraso.Location = new System.Drawing.Point(17, 381);
            this.labelTarefasEmAtraso.Name = "labelTarefasEmAtraso";
            this.labelTarefasEmAtraso.Size = new System.Drawing.Size(142, 20);
            this.labelTarefasEmAtraso.TabIndex = 4;
            this.labelTarefasEmAtraso.Text = "Tarefas em atraso:";
            // 
            // labelResponsavelProjecto
            // 
            this.labelResponsavelProjecto.AutoSize = true;
            this.labelResponsavelProjecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResponsavelProjecto.Location = new System.Drawing.Point(12, 34);
            this.labelResponsavelProjecto.Name = "labelResponsavelProjecto";
            this.labelResponsavelProjecto.Size = new System.Drawing.Size(143, 25);
            this.labelResponsavelProjecto.TabIndex = 5;
            this.labelResponsavelProjecto.Text = "Responsável:";
            // 
            // responsavelProjecto
            // 
            this.responsavelProjecto.AutoSize = true;
            this.responsavelProjecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.responsavelProjecto.Location = new System.Drawing.Point(161, 34);
            this.responsavelProjecto.Name = "responsavelProjecto";
            this.responsavelProjecto.Size = new System.Drawing.Size(236, 25);
            this.responsavelProjecto.TabIndex = 6;
            this.responsavelProjecto.Text = "<responsável projecto>";
            // 
            // labelTipoProjecto
            // 
            this.labelTipoProjecto.AutoSize = true;
            this.labelTipoProjecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipoProjecto.Location = new System.Drawing.Point(12, 59);
            this.labelTipoProjecto.Name = "labelTipoProjecto";
            this.labelTipoProjecto.Size = new System.Drawing.Size(60, 25);
            this.labelTipoProjecto.TabIndex = 7;
            this.labelTipoProjecto.Text = "Tipo:";
            // 
            // tipoProjecto
            // 
            this.tipoProjecto.AutoSize = true;
            this.tipoProjecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tipoProjecto.Location = new System.Drawing.Point(78, 59);
            this.tipoProjecto.Name = "tipoProjecto";
            this.tipoProjecto.Size = new System.Drawing.Size(236, 25);
            this.tipoProjecto.TabIndex = 8;
            this.tipoProjecto.Text = "<responsável projecto>";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabelaRecursos);
            this.panel3.Location = new System.Drawing.Point(17, 675);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(775, 228);
            this.panel3.TabIndex = 9;
            // 
            // tabelaRecursos
            // 
            this.tabelaRecursos.AllowUserToAddRows = false;
            this.tabelaRecursos.AllowUserToDeleteRows = false;
            this.tabelaRecursos.AutoGenerateColumns = false;
            this.tabelaRecursos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaRecursos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.descricaoDataGridViewTextBoxColumn2,
            this.idElementoAssociadoDataGridViewTextBoxColumn});
            this.tabelaRecursos.DataSource = this.recursoBindingSource;
            this.tabelaRecursos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabelaRecursos.Location = new System.Drawing.Point(0, 0);
            this.tabelaRecursos.Name = "tabelaRecursos";
            this.tabelaRecursos.ReadOnly = true;
            this.tabelaRecursos.Size = new System.Drawing.Size(775, 228);
            this.tabelaRecursos.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // descricaoDataGridViewTextBoxColumn2
            // 
            this.descricaoDataGridViewTextBoxColumn2.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn2.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn2.Name = "descricaoDataGridViewTextBoxColumn2";
            this.descricaoDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // idElementoAssociadoDataGridViewTextBoxColumn
            // 
            this.idElementoAssociadoDataGridViewTextBoxColumn.DataPropertyName = "IdElementoAssociado";
            this.idElementoAssociadoDataGridViewTextBoxColumn.HeaderText = "IdElementoAssociado";
            this.idElementoAssociadoDataGridViewTextBoxColumn.Name = "idElementoAssociadoDataGridViewTextBoxColumn";
            this.idElementoAssociadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // recursoBindingSource
            // 
            this.recursoBindingSource.DataSource = typeof(BO.Recurso);
            // 
            // labelRecursos
            // 
            this.labelRecursos.AutoSize = true;
            this.labelRecursos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecursos.Location = new System.Drawing.Point(17, 652);
            this.labelRecursos.Name = "labelRecursos";
            this.labelRecursos.Size = new System.Drawing.Size(151, 20);
            this.labelRecursos.TabIndex = 10;
            this.labelRecursos.Text = "Recursos utilizados:";
            // 
            // Relatorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 921);
            this.Controls.Add(this.labelRecursos);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tipoProjecto);
            this.Controls.Add(this.labelTipoProjecto);
            this.Controls.Add(this.responsavelProjecto);
            this.Controls.Add(this.labelResponsavelProjecto);
            this.Controls.Add(this.labelTarefasEmAtraso);
            this.Controls.Add(this.labelTarefasTerminadas);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.nomeProjecto);
            this.Controls.Add(this.labelNomeProjecto);
            this.Name = "Relatorio";
            this.Text = "Relatório do Projecto";
            this.Load += new System.EventHandler(this.OnInit);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaTarefasTerminadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarefasTerminadasBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaTarefasEmAtraso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarefasEmAtraso)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaRecursos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recursoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNomeProjecto;
        private System.Windows.Forms.Label nomeProjecto;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView tabelaTarefasTerminadas;
        private System.Windows.Forms.BindingSource tarefasTerminadasBindingSource;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView tabelaTarefasEmAtraso;
        private System.Windows.Forms.BindingSource tarefasEmAtraso;
        private System.Windows.Forms.Label labelTarefasTerminadas;
        private System.Windows.Forms.Label labelTarefasEmAtraso;
        private System.Windows.Forms.Label labelResponsavelProjecto;
        private System.Windows.Forms.Label responsavelProjecto;
        private System.Windows.Forms.Label labelTipoProjecto;
        private System.Windows.Forms.Label tipoProjecto;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idTarefaPaiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProjectoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fim;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idTarefaPaiDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProjectoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView tabelaRecursos;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idElementoAssociadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource recursoBindingSource;
        private System.Windows.Forms.Label labelRecursos;
    }
}