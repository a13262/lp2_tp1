﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BO;

namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Projectos : Form
    {
        /// <summary>
        /// The data binding source loaded
        /// </summary>
        private bool dataBindingSourceLoaded = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Projectos"/> class.
        /// </summary>
        public Projectos()
        {
            InitializeComponent();
            tipoDataGridViewTextBoxColumn.DataSource = Enum.GetValues(typeof(Projecto.TipoProjecto));
        }

        /// <summary>
        /// Gets or sets a value indicating whether [data binding source loaded].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [data binding source loaded]; otherwise, <c>false</c>.
        /// </value>
        public bool DataBindingSourceLoaded
        {
            get { return dataBindingSourceLoaded; }
            set { dataBindingSourceLoaded = value; }
        }

        /// <summary>
        /// Called when [initialize].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnInit(object sender, EventArgs e)
        {
            inicio.Visible = false;
            fim.Visible = false;

            try
            {
                var pbll = new ProjectoPL();
                foreach (Projecto p in pbll.Read())
                {
                    projectoBindingSource.Add(p);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            finally
            {
                DataBindingSourceLoaded = true;
                projectoBindingSource.ResetBindings(false);
            }
            
        }

        /// <summary>
        /// Called when [cell click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void OnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                switch (e.ColumnIndex)
                {
                    case 6:
                        if (projectoBindingSource.Current is Projecto p1)
                        {
                            var tarefas = new Tarefas(p1.Id);
                            var result = tarefas.ShowDialog();
                        }
                        break;
                    case 5:
                        if (projectoBindingSource.Current is Projecto p2)
                        {
                            var pessoas = new Pessoas(p2.IdResponsavel, p2.Id);
                            pessoas.Owner = this;
                            var result = pessoas.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                p2.IdResponsavel = pessoas.IdResponsavel;
                                /*Por algum motivo o evento ListChanged não está a ser despoletado, então
                                 é necessário chamar manualmente a função associada ao evento
                                */
                                OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, projectoBindingSource.IndexOf(p2)));
                            }
                        }
                        break;
                    case 7:
                        if (projectoBindingSource.Current is Projecto p3)
                        {
                            var recursos = new Recursos(p3.Id);
                            recursos.Owner = this;
                            var result = recursos.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, projectoBindingSource.IndexOf(p3)));
                            }
                        }
                        break;
                    case 2:
                        inicio.Visible = true;
                        inicio.Select();
                        SendKeys.Send("%{DOWN}");
                        break;
                    case 3:
                        fim.Visible = true;
                        fim.Select();
                        SendKeys.Send("%{DOWN}");
                        break;
                    case 8:
                        if (projectoBindingSource.Current is Projecto p4)
                            new Relatorio(p4.Id).ShowDialog();
                        break;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            
        }   

        /// <summary>
        /// Called when [inicio close up].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnInicioCloseUp(object sender, EventArgs e)
        {
            try
            {
                if (projectoBindingSource.Current is Projecto p)
                {
                    p.Inicio = inicio.Value;
                    OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, projectoBindingSource.IndexOf(p)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }finally { inicio.Visible = false; }
            
            

            
        }

        /// <summary>
        /// Called when [fim close up].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnFimCloseUp(object sender, EventArgs e)
        {
            try
            {
                if (projectoBindingSource.Current is Projecto p)
                {
                    p.Fim = fim.Value;
                    OnBindingSource_ListChanged(null,
                        new ListChangedEventArgs(ListChangedType.ItemChanged, projectoBindingSource.IndexOf(p)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            finally
            {
                fim.Visible = false;
            }
            
            
        }

        /// <summary>
        /// Called when [novo click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnNovoClick(object sender, EventArgs e)
        {
            projectoBindingSource.Add(new Projecto());
        }

        /// <summary>
        /// Called when [binding source list changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ListChangedEventArgs"/> instance containing the event data.</param>
        private void OnBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                if (DataBindingSourceLoaded)
                {
                    var ppl = new ProjectoPL();

                    if (e.ListChangedType == ListChangedType.ItemChanged)
                    {
                        Projecto p = projectoBindingSource[e.NewIndex] as Projecto;
                        if (ppl.Update(p))
                        {
                            MessageBox.Show("Registo actualizado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao actualizar o registo!");
                        }

                    }
                    else if (e.ListChangedType == ListChangedType.ItemAdded)
                    {
                        Projecto p = projectoBindingSource[e.NewIndex] as Projecto;
                        if (ppl.Create(p))
                        {
                            if (dataBindingSourceLoaded) MessageBox.Show("Registo criado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao criar o registo!");
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
        }

        /// <summary>
        /// Handles the UserDeletingRow event of the tabelaProjectos control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewRowCancelEventArgs"/> instance containing the event data.</param>
        private void tabelaProjectos_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                Projecto p = e.Row.DataBoundItem as Projecto;
                var ppl = new ProjectoPL();
                if (ppl.Delete(p.Id))
                {
                    MessageBox.Show("Registo apagado com sucesso!");
                }
                else
                {
                    MessageBox.Show("Falha ao apagar o registo!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
        }
    }
}
