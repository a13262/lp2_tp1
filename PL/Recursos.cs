﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BO;

namespace PL
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Recursos : Form
    {

        /// <summary>
        /// The data binding source loaded
        /// </summary>
        private bool dataBindingSourceLoaded = false;

        /// <summary>
        /// The identifier elemento assoc
        /// </summary>
        private Guid idElementoAssoc;


        /// <summary>
        /// Initializes a new instance of the <see cref="Recursos" /> class.
        /// </summary>
        public Recursos()
        {
            InitializeComponent();
            
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Recursos"/> class.
        /// </summary>
        /// <param name="idElementoAssoc">The identifier elemento assoc.</param>
        public Recursos(Guid idElementoAssoc)
        {
            InitializeComponent();
            IdElementoAssoc = idElementoAssoc;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [data binding source loaded].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [data binding source loaded]; otherwise, <c>false</c>.
        /// </value>
        public bool DataBindingSourceLoaded
        {
            get { return dataBindingSourceLoaded; }
            set { dataBindingSourceLoaded = value; }
        }

        /// <summary>
        /// Gets or sets the identifier elemento assoc.
        /// </summary>
        /// <value>
        /// The identifier elemento assoc.
        /// </value>
        public Guid IdElementoAssoc
        {
            get { return idElementoAssoc; }
            set { idElementoAssoc = value; }
        }

        /// <summary>
        /// Handles the FormClosed event of the Recursos control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosedEventArgs" /> instance containing the event data.</param>
        private void OnClose(object sender, FormClosedEventArgs e)
        {
            /*
            var rbll = new RecursoBLL();
            foreach (Recurso r in recursoBindingSource)
            {
                rbll.Create(r);
            }
            */
        }

        /// <summary>
        /// Hides the project resources.
        /// </summary>
        private void HideProjectResources()
        {
            splitContainer.Panel2.Visible = false;
            splitContainer.Panel1.Dock = DockStyle.Fill;
            labelRecursos.Text = "Recursos existentes";
        }

        /// <summary>
        /// Handles the Load event of the Recursos control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnInit(object sender, EventArgs e)
        {
            try
            {
                var rbll = new RecursoPL();

                if (this.Owner == null) {
                    
                    HideProjectResources();

                    foreach (Recurso r in rbll.Read(true))
                    {
                        recursoBindingSource.Add(r);
                    }
                }
                else
                {
                    //Adiciona os recursos disponíveis / por alocar
                    foreach (Recurso r in rbll.Read())
                    {
                       recursoBindingSource.Add(r);
                        
                    }

                    //Adiciona os recursos alocados ao projecto / tarefa
                    rbll.IdElementoAssoc = IdElementoAssoc;

                    foreach (Recurso r in rbll.Read())
                    {
                        recursoProjectoBindingSource.Add(r);
            
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            finally
            {
                DataBindingSourceLoaded = true;
                recursoBindingSource.ResetBindings(false);
                recursoProjectoBindingSource.ResetBindings(false);
            }

        }

        /// <summary>
        /// Handles the ListChanged event of the RecursoBindingSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ListChangedEventArgs" /> instance containing the event data.</param>
        private void OnBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                if (DataBindingSourceLoaded)
                {
                    var rpl = new RecursoPL();

                    if (e.ListChangedType == ListChangedType.ItemChanged)
                    {
                        Recurso r = recursoBindingSource[e.NewIndex] as Recurso;
                        if (rpl.Update(r))
                        {
                            MessageBox.Show("Recurso actualizado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao actualizar o recurso!");
                        }
                        
                    }
                    else if (e.ListChangedType == ListChangedType.ItemAdded)
                    {
                        Recurso r = recursoBindingSource[e.NewIndex] as Recurso;
                        if (rpl.Create(r))
                        {
                            if(dataBindingSourceLoaded) MessageBox.Show("Recurso criado com sucesso!");
                        }
                        else
                        {
                            MessageBox.Show("Falha ao criar o recurso!");
                        }

                     }
                    
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            
        }

        /// <summary>
        /// Called when [user deleting row].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataGridViewRowCancelEventArgs"/> instance containing the event data.</param>
        private void OnUserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                Recurso r = e.Row.DataBoundItem as Recurso;
                var rpl = new RecursoPL();
                if (rpl.Delete(r.Id))
                {
                    MessageBox.Show("Recurso apagado com sucesso!");
                }
                else
                {
                    MessageBox.Show("Falha ao apagar o recurso: existem dependências!");
                    this.Close();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            

        }

        /// <summary>
        /// Called when [novo click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnNovoClick(object sender, EventArgs e)
        {
            recursoBindingSource.Add(new Recurso());
        }

        /// <summary>
        /// Called when [cell double click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void OnCellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.Owner == null) return;
                switch (e.ColumnIndex)
                {
                    case 0:
                        if (recursoBindingSource.Current is Recurso r)
                        {
                            r.IdElementoAssociado = IdElementoAssoc;
                            OnBindingSource_ListChanged(null, new ListChangedEventArgs(ListChangedType.ItemChanged, recursoBindingSource.IndexOf(r)));
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                        }
                        break;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
            
        }

        /// <summary>
        /// Handles the CellDoubleClick event of the tabelaRecursosProjecto control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void tabelaRecursosProjecto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.Owner == null) return;
                switch (e.ColumnIndex)
                {
                    case 0:
                        if (recursoProjectoBindingSource.Current is Recurso r)
                        {
                            var rpl = new RecursoPL();
                            if (rpl.Free(r.Id))
                            {
                                MessageBox.Show("Recurso libertado!");
                                this.Close();
                            }
                        }
                        break;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }

        }
    }
}
